﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1366199518;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissShotController
struct  MissShotController_t1066454504  : public MonoBehaviour_t774292115
{
public:
	// UnityEngine.GameObject MissShotController::playerLeft
	GameObject_t1366199518 * ___playerLeft_3;
	// UnityEngine.GameObject MissShotController::playerRight
	GameObject_t1366199518 * ___playerRight_4;
	// System.Boolean MissShotController::WaitForSpawnNewCowboyStarted
	bool ___WaitForSpawnNewCowboyStarted_5;

public:
	inline static int32_t get_offset_of_playerLeft_3() { return static_cast<int32_t>(offsetof(MissShotController_t1066454504, ___playerLeft_3)); }
	inline GameObject_t1366199518 * get_playerLeft_3() const { return ___playerLeft_3; }
	inline GameObject_t1366199518 ** get_address_of_playerLeft_3() { return &___playerLeft_3; }
	inline void set_playerLeft_3(GameObject_t1366199518 * value)
	{
		___playerLeft_3 = value;
		Il2CppCodeGenWriteBarrier(&___playerLeft_3, value);
	}

	inline static int32_t get_offset_of_playerRight_4() { return static_cast<int32_t>(offsetof(MissShotController_t1066454504, ___playerRight_4)); }
	inline GameObject_t1366199518 * get_playerRight_4() const { return ___playerRight_4; }
	inline GameObject_t1366199518 ** get_address_of_playerRight_4() { return &___playerRight_4; }
	inline void set_playerRight_4(GameObject_t1366199518 * value)
	{
		___playerRight_4 = value;
		Il2CppCodeGenWriteBarrier(&___playerRight_4, value);
	}

	inline static int32_t get_offset_of_WaitForSpawnNewCowboyStarted_5() { return static_cast<int32_t>(offsetof(MissShotController_t1066454504, ___WaitForSpawnNewCowboyStarted_5)); }
	inline bool get_WaitForSpawnNewCowboyStarted_5() const { return ___WaitForSpawnNewCowboyStarted_5; }
	inline bool* get_address_of_WaitForSpawnNewCowboyStarted_5() { return &___WaitForSpawnNewCowboyStarted_5; }
	inline void set_WaitForSpawnNewCowboyStarted_5(bool value)
	{
		___WaitForSpawnNewCowboyStarted_5 = value;
	}
};

struct MissShotController_t1066454504_StaticFields
{
public:
	// System.Boolean MissShotController::waitForShotStarted
	bool ___waitForShotStarted_2;

public:
	inline static int32_t get_offset_of_waitForShotStarted_2() { return static_cast<int32_t>(offsetof(MissShotController_t1066454504_StaticFields, ___waitForShotStarted_2)); }
	inline bool get_waitForShotStarted_2() const { return ___waitForShotStarted_2; }
	inline bool* get_address_of_waitForShotStarted_2() { return &___waitForShotStarted_2; }
	inline void set_waitForShotStarted_2(bool value)
	{
		___waitForShotStarted_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
