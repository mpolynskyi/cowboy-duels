﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameStateManager
struct  GameStateManager_t1063875770  : public MonoBehaviour_t774292115
{
public:

public:
};

struct GameStateManager_t1063875770_StaticFields
{
public:
	// System.Int32 GameStateManager::maxScore
	int32_t ___maxScore_2;
	// System.Int32 GameStateManager::_currentGameState
	int32_t ____currentGameState_3;
	// System.Int32 GameStateManager::cowboysReadyForDuel
	int32_t ___cowboysReadyForDuel_4;
	// System.Int32 GameStateManager::scoreLeft
	int32_t ___scoreLeft_5;
	// System.Int32 GameStateManager::scoreRight
	int32_t ___scoreRight_6;
	// System.Int32 GameStateManager::needToSpawn
	int32_t ___needToSpawn_7;
	// System.Int32 GameStateManager::_randomDelay
	int32_t ____randomDelay_8;

public:
	inline static int32_t get_offset_of_maxScore_2() { return static_cast<int32_t>(offsetof(GameStateManager_t1063875770_StaticFields, ___maxScore_2)); }
	inline int32_t get_maxScore_2() const { return ___maxScore_2; }
	inline int32_t* get_address_of_maxScore_2() { return &___maxScore_2; }
	inline void set_maxScore_2(int32_t value)
	{
		___maxScore_2 = value;
	}

	inline static int32_t get_offset_of__currentGameState_3() { return static_cast<int32_t>(offsetof(GameStateManager_t1063875770_StaticFields, ____currentGameState_3)); }
	inline int32_t get__currentGameState_3() const { return ____currentGameState_3; }
	inline int32_t* get_address_of__currentGameState_3() { return &____currentGameState_3; }
	inline void set__currentGameState_3(int32_t value)
	{
		____currentGameState_3 = value;
	}

	inline static int32_t get_offset_of_cowboysReadyForDuel_4() { return static_cast<int32_t>(offsetof(GameStateManager_t1063875770_StaticFields, ___cowboysReadyForDuel_4)); }
	inline int32_t get_cowboysReadyForDuel_4() const { return ___cowboysReadyForDuel_4; }
	inline int32_t* get_address_of_cowboysReadyForDuel_4() { return &___cowboysReadyForDuel_4; }
	inline void set_cowboysReadyForDuel_4(int32_t value)
	{
		___cowboysReadyForDuel_4 = value;
	}

	inline static int32_t get_offset_of_scoreLeft_5() { return static_cast<int32_t>(offsetof(GameStateManager_t1063875770_StaticFields, ___scoreLeft_5)); }
	inline int32_t get_scoreLeft_5() const { return ___scoreLeft_5; }
	inline int32_t* get_address_of_scoreLeft_5() { return &___scoreLeft_5; }
	inline void set_scoreLeft_5(int32_t value)
	{
		___scoreLeft_5 = value;
	}

	inline static int32_t get_offset_of_scoreRight_6() { return static_cast<int32_t>(offsetof(GameStateManager_t1063875770_StaticFields, ___scoreRight_6)); }
	inline int32_t get_scoreRight_6() const { return ___scoreRight_6; }
	inline int32_t* get_address_of_scoreRight_6() { return &___scoreRight_6; }
	inline void set_scoreRight_6(int32_t value)
	{
		___scoreRight_6 = value;
	}

	inline static int32_t get_offset_of_needToSpawn_7() { return static_cast<int32_t>(offsetof(GameStateManager_t1063875770_StaticFields, ___needToSpawn_7)); }
	inline int32_t get_needToSpawn_7() const { return ___needToSpawn_7; }
	inline int32_t* get_address_of_needToSpawn_7() { return &___needToSpawn_7; }
	inline void set_needToSpawn_7(int32_t value)
	{
		___needToSpawn_7 = value;
	}

	inline static int32_t get_offset_of__randomDelay_8() { return static_cast<int32_t>(offsetof(GameStateManager_t1063875770_StaticFields, ____randomDelay_8)); }
	inline int32_t get__randomDelay_8() const { return ____randomDelay_8; }
	inline int32_t* get_address_of__randomDelay_8() { return &____randomDelay_8; }
	inline void set__randomDelay_8(int32_t value)
	{
		____randomDelay_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
