﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PrepareToDuel
struct PrepareToDuel_t908926924;

#include "codegen/il2cpp-codegen.h"

// System.Void PrepareToDuel::.ctor()
extern "C"  void PrepareToDuel__ctor_m939879657 (PrepareToDuel_t908926924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrepareToDuel::.cctor()
extern "C"  void PrepareToDuel__cctor_m2023699102 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PrepareToDuel::get_playerStoped()
extern "C"  bool PrepareToDuel_get_playerStoped_m3146878400 (PrepareToDuel_t908926924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrepareToDuel::set_playerStoped(System.Boolean)
extern "C"  void PrepareToDuel_set_playerStoped_m2692168537 (PrepareToDuel_t908926924 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrepareToDuel::Start()
extern "C"  void PrepareToDuel_Start_m712267537 (PrepareToDuel_t908926924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrepareToDuel::Update()
extern "C"  void PrepareToDuel_Update_m4209704278 (PrepareToDuel_t908926924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PrepareToDuel::CalculateStopPoints()
extern "C"  void PrepareToDuel_CalculateStopPoints_m2830488464 (PrepareToDuel_t908926924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
