﻿using UnityEngine;
using System.Collections;

public class GameStateManager : MonoBehaviour {

    //starting states of the game
    public enum DuelStates {start_game, spawn_cowboy, cowboys_move_to_center, cowboys_ready, cowboys_aim_guns, after_shot, end_game};
    public static int maxScore = 5;
    private static int _currentGameState;
    public static int currentGameState
    {
        get
        {
            //print("current game state is: " + _currentGameState);
            return _currentGameState;
        }
        set
        {
            _currentGameState = value;
        }
    }
    public static int cowboysReadyForDuel = 0;
    public static int scoreLeft = 0;
    public static int scoreRight = 0;

    public enum WhoToSpawn {left, right, both};
    public static int needToSpawn = (int)WhoToSpawn.both;  //to spawn both cowboys at game start 

    private static int _randomDelay = 0;
    public static int RandomDelay
    {
        get
        {
            if (_randomDelay == 0)
            {
                _randomDelay = Random.Range(3, 10);
            }
            return _randomDelay;
        }
        set { _randomDelay = value; }
    }

    
    public static int CowboysOnScene
    {
        get
        {
            int cowboys = 0;
            if (GameObject.FindWithTag("PlayerLeft"))
            {
                cowboys++;
            }
            if (GameObject.FindWithTag("PlayerRight"))
            {
                cowboys++;
            }
            return cowboys;

        }
    }

    public static void ResetRandomDelay()
    {
        _randomDelay = 0;
    }
}
