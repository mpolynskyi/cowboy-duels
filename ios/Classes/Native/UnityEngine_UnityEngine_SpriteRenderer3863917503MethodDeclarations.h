﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3863917503;
// UnityEngine.Sprite
struct Sprite_t1118776648;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Sprite1118776648.h"

// UnityEngine.Sprite UnityEngine.SpriteRenderer::get_sprite()
extern "C"  Sprite_t1118776648 * SpriteRenderer_get_sprite_m3016837432 (SpriteRenderer_t3863917503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
extern "C"  void SpriteRenderer_set_sprite_m617298623 (SpriteRenderer_t3863917503 * __this, Sprite_t1118776648 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.SpriteRenderer::GetSprite_INTERNAL()
extern "C"  Sprite_t1118776648 * SpriteRenderer_GetSprite_INTERNAL_m2252122683 (SpriteRenderer_t3863917503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::SetSprite_INTERNAL(UnityEngine.Sprite)
extern "C"  void SpriteRenderer_SetSprite_INTERNAL_m2338909670 (SpriteRenderer_t3863917503 * __this, Sprite_t1118776648 * ___sprite, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::set_flipX(System.Boolean)
extern "C"  void SpriteRenderer_set_flipX_m994174828 (SpriteRenderer_t3863917503 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
