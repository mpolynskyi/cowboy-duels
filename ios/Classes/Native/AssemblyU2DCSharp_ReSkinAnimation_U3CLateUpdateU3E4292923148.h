﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object707969140.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReSkinAnimation/<LateUpdate>c__AnonStorey6
struct  U3CLateUpdateU3Ec__AnonStorey6_t4292923148  : public Il2CppObject
{
public:
	// System.String ReSkinAnimation/<LateUpdate>c__AnonStorey6::spriteName
	String_t* ___spriteName_0;

public:
	inline static int32_t get_offset_of_spriteName_0() { return static_cast<int32_t>(offsetof(U3CLateUpdateU3Ec__AnonStorey6_t4292923148, ___spriteName_0)); }
	inline String_t* get_spriteName_0() const { return ___spriteName_0; }
	inline String_t** get_address_of_spriteName_0() { return &___spriteName_0; }
	inline void set_spriteName_0(String_t* value)
	{
		___spriteName_0 = value;
		Il2CppCodeGenWriteBarrier(&___spriteName_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
