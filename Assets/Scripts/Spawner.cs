﻿using UnityEngine;

public class Spawner : MonoBehaviour {
    public GameObject cowboyPrefab;
    public static int cowboysSpawned = 0;


    void Update () {
	    if(GameStateManager.currentGameState == (int)GameStateManager.DuelStates.spawn_cowboy)
        {
            Shot.alreadyShot = false; //reset for next spawned cowboys. It placed here because this game don't have good code architecture.
            switch (GameStateManager.needToSpawn)
            {
                case (int)GameStateManager.WhoToSpawn.left:
                    SpawnLeftCowboy();
                    break;
                case (int)GameStateManager.WhoToSpawn.right:
                    SpawnRightCowboy();
                    break;
                case (int)GameStateManager.WhoToSpawn.both:
                    SpawnLeftCowboy();
                    SpawnRightCowboy();
                    break;
            }
            
            
            if (GameStateManager.CowboysOnScene == 2)
            {
                GameStateManager.currentGameState = (int)GameStateManager.DuelStates.cowboys_move_to_center;
            }
            
        }
	}
    void SpawnLeftCowboy ()
    {
        if(gameObject.tag == "SpawnerLeft")
        {
            var leftCowboyReskin = Instantiate(cowboyPrefab).GetComponent<ReSkinAnimation>();
            leftCowboyReskin.spriteSheetName = "Cowboy" + Random.Range(1, 8);  //1-7 numbers of spritesheets with cowboys. 8 - because Range set between 1 and 7 gives a returned values between 1 and 6.
        }
    }
    void SpawnRightCowboy()
    {
        if (gameObject.tag == "SpawnerRight")
        {
            var rightCowboyReskin = Instantiate(cowboyPrefab).GetComponent<ReSkinAnimation>();
            rightCowboyReskin.spriteSheetName = "Cowboy" + Random.Range(1, 8);  //1-7 numbers of spritesheets with cowboys. 8 - because Range set between 1 and 7 gives a returned values between 1 and 6.
        }
    }
}
