﻿using UnityEngine;
using UnityEngine.UI;

public class ShowScore : MonoBehaviour {
    [SerializeField]
    private Text leftScore;

    [SerializeField]
    private Text rightScore;
	
	void Start ()
    {
        leftScore.gameObject.SetActive(false);
        rightScore.gameObject.SetActive(false);
    }

	void Update () {
	    if(GameStateManager.currentGameState != (int)GameStateManager.DuelStates.start_game)
        {
            leftScore.gameObject.SetActive(true);
            rightScore.gameObject.SetActive(true);
            leftScore.text = GameStateManager.scoreLeft.ToString();
            rightScore.text = GameStateManager.scoreRight.ToString();
        }

	}
}
