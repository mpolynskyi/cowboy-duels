﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"
#include "UnityEngine_UnityEngine_Quaternion83606849.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rotator
struct  Rotator_t3375904803  : public MonoBehaviour_t774292115
{
public:
	// System.Single Rotator::angle
	float ___angle_2;
	// System.Single Rotator::speed
	float ___speed_3;
	// UnityEngine.Quaternion Rotator::qStart
	Quaternion_t83606849  ___qStart_4;
	// UnityEngine.Quaternion Rotator::qEnd
	Quaternion_t83606849  ___qEnd_5;

public:
	inline static int32_t get_offset_of_angle_2() { return static_cast<int32_t>(offsetof(Rotator_t3375904803, ___angle_2)); }
	inline float get_angle_2() const { return ___angle_2; }
	inline float* get_address_of_angle_2() { return &___angle_2; }
	inline void set_angle_2(float value)
	{
		___angle_2 = value;
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(Rotator_t3375904803, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_qStart_4() { return static_cast<int32_t>(offsetof(Rotator_t3375904803, ___qStart_4)); }
	inline Quaternion_t83606849  get_qStart_4() const { return ___qStart_4; }
	inline Quaternion_t83606849 * get_address_of_qStart_4() { return &___qStart_4; }
	inline void set_qStart_4(Quaternion_t83606849  value)
	{
		___qStart_4 = value;
	}

	inline static int32_t get_offset_of_qEnd_5() { return static_cast<int32_t>(offsetof(Rotator_t3375904803, ___qEnd_5)); }
	inline Quaternion_t83606849  get_qEnd_5() const { return ___qEnd_5; }
	inline Quaternion_t83606849 * get_address_of_qEnd_5() { return &___qEnd_5; }
	inline void set_qEnd_5(Quaternion_t83606849  value)
	{
		___qEnd_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
