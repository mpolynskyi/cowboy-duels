﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour
{
    public float angle;
    public float speed;

    Quaternion qStart, qEnd;

    void Start()
    {
        qStart = Quaternion.AngleAxis(angle, Vector3.forward);
        qEnd = Quaternion.AngleAxis(-angle, Vector3.forward);
    }

    void Update()
    {
        transform.rotation = Quaternion.Lerp(qStart, qEnd, (Mathf.Sin(Time.time * speed) + 1.0f) / 2.0f);
    }
}
