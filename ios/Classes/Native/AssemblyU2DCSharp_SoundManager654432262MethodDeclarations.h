﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SoundManager
struct SoundManager_t654432262;
// UnityEngine.AudioClip
struct AudioClip_t3927647597;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t3948543552;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AudioClip3927647597.h"

// System.Void SoundManager::.ctor()
extern "C"  void SoundManager__ctor_m3417712111 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::.cctor()
extern "C"  void SoundManager__cctor_m1047370624 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::Awake()
extern "C"  void SoundManager_Awake_m1006343474 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::PlaySingle(UnityEngine.AudioClip)
extern "C"  void SoundManager_PlaySingle_m2017509048 (SoundManager_t654432262 * __this, AudioClip_t3927647597 * ___clip, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::RandomizeSfx(UnityEngine.AudioClip[])
extern "C"  void SoundManager_RandomizeSfx_m3482976376 (SoundManager_t654432262 * __this, AudioClipU5BU5D_t3948543552* ___clips, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::playCowboysMoveToCenterMusic()
extern "C"  void SoundManager_playCowboysMoveToCenterMusic_m3336171715 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::PlaySuicideSound()
extern "C"  void SoundManager_PlaySuicideSound_m707867868 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::PlayFire()
extern "C"  void SoundManager_PlayFire_m1786007431 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::PlayWinStateMusic()
extern "C"  void SoundManager_PlayWinStateMusic_m2740338453 (SoundManager_t654432262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
