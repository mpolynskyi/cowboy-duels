﻿using UnityEngine;
using System.Collections;

public class Death : MonoBehaviour {
    public AudioClip deathAudio1;

    public void Die () {
        Destroy(gameObject);
    }
    public void Disable()
    {
        GetComponent<SpriteRenderer>().enabled = false;
    }
}
