﻿using UnityEngine;
using System;

public class ReSkinAnimation : MonoBehaviour {

    public string spriteSheetName;
    public BoxCollider2D dynamicCollider;
    void Awake()
    {
        dynamicCollider = GetComponent<BoxCollider2D>();
    }

	void LateUpdate () {
        var subSprites = Resources.LoadAll<Sprite>("Artwork/sprite-sheets/" + spriteSheetName);

        foreach (var renderer in GetComponentsInChildren<SpriteRenderer>())
        {
            string spriteName = renderer.sprite.name;
            var newSprite = Array.Find(subSprites, item => item.name == spriteName);

            if (newSprite)
            {
                renderer.sprite = newSprite;
                dynamicCollider.size = newSprite.bounds.size;
            }
        }
	}
}
