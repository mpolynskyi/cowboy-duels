﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1366199518;
// UnityEngine.AudioClip
struct AudioClip_t3927647597;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StartGame
struct  StartGame_t415325182  : public MonoBehaviour_t774292115
{
public:
	// UnityEngine.AudioClip StartGame::startMusic
	AudioClip_t3927647597 * ___startMusic_3;
	// System.Boolean StartGame::musicStarted
	bool ___musicStarted_4;

public:
	inline static int32_t get_offset_of_startMusic_3() { return static_cast<int32_t>(offsetof(StartGame_t415325182, ___startMusic_3)); }
	inline AudioClip_t3927647597 * get_startMusic_3() const { return ___startMusic_3; }
	inline AudioClip_t3927647597 ** get_address_of_startMusic_3() { return &___startMusic_3; }
	inline void set_startMusic_3(AudioClip_t3927647597 * value)
	{
		___startMusic_3 = value;
		Il2CppCodeGenWriteBarrier(&___startMusic_3, value);
	}

	inline static int32_t get_offset_of_musicStarted_4() { return static_cast<int32_t>(offsetof(StartGame_t415325182, ___musicStarted_4)); }
	inline bool get_musicStarted_4() const { return ___musicStarted_4; }
	inline bool* get_address_of_musicStarted_4() { return &___musicStarted_4; }
	inline void set_musicStarted_4(bool value)
	{
		___musicStarted_4 = value;
	}
};

struct StartGame_t415325182_StaticFields
{
public:
	// UnityEngine.GameObject StartGame::tapText
	GameObject_t1366199518 * ___tapText_2;

public:
	inline static int32_t get_offset_of_tapText_2() { return static_cast<int32_t>(offsetof(StartGame_t415325182_StaticFields, ___tapText_2)); }
	inline GameObject_t1366199518 * get_tapText_2() const { return ___tapText_2; }
	inline GameObject_t1366199518 ** get_address_of_tapText_2() { return &___tapText_2; }
	inline void set_tapText_2(GameObject_t1366199518 * value)
	{
		___tapText_2 = value;
		Il2CppCodeGenWriteBarrier(&___tapText_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
