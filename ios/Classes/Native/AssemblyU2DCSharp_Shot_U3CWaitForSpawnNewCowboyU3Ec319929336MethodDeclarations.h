﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Shot/<WaitForSpawnNewCowboy>c__Iterator3
struct U3CWaitForSpawnNewCowboyU3Ec__Iterator3_t319929336;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Shot/<WaitForSpawnNewCowboy>c__Iterator3::.ctor()
extern "C"  void U3CWaitForSpawnNewCowboyU3Ec__Iterator3__ctor_m1955491998 (U3CWaitForSpawnNewCowboyU3Ec__Iterator3_t319929336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Shot/<WaitForSpawnNewCowboy>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForSpawnNewCowboyU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1731888494 (U3CWaitForSpawnNewCowboyU3Ec__Iterator3_t319929336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Shot/<WaitForSpawnNewCowboy>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForSpawnNewCowboyU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1977705126 (U3CWaitForSpawnNewCowboyU3Ec__Iterator3_t319929336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Shot/<WaitForSpawnNewCowboy>c__Iterator3::MoveNext()
extern "C"  bool U3CWaitForSpawnNewCowboyU3Ec__Iterator3_MoveNext_m1487698266 (U3CWaitForSpawnNewCowboyU3Ec__Iterator3_t319929336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shot/<WaitForSpawnNewCowboy>c__Iterator3::Dispose()
extern "C"  void U3CWaitForSpawnNewCowboyU3Ec__Iterator3_Dispose_m2695425855 (U3CWaitForSpawnNewCowboyU3Ec__Iterator3_t319929336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shot/<WaitForSpawnNewCowboy>c__Iterator3::Reset()
extern "C"  void U3CWaitForSpawnNewCowboyU3Ec__Iterator3_Reset_m3401338405 (U3CWaitForSpawnNewCowboyU3Ec__Iterator3_t319929336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
