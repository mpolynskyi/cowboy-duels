﻿using UnityEngine;
using System.Collections;

public class PrepareToDuel : MonoBehaviour
{
    public float pixelsFromCenter;
    public float speed;
    private Vector2 sideTo = new Vector2();
    private float stopPointX;
    AnimationManager animator;
    bool cowboys_move_to_center_started = false;


    private static bool playerStopedController = false;
    private bool _playerStoped;
    bool playerStoped {
        get
        {
            if (!playerStopedController)
            {
                return false;
            }
            else
            {
                return _playerStoped;
            }
        }
        set
        {
            _playerStoped = value;
        }
    }

    void Start()
    {
        CalculateStopPoints();
    }

    void Update()
    {

        //move cowboys to center and change state to cowboys_ready at the end
        if (GameStateManager.currentGameState == (int)GameStateManager.DuelStates.cowboys_move_to_center)
        {
            if (animator == null)
            {
                animator = GetComponent<AnimationManager>();
            }

            

            if (Mathf.Abs(transform.position.x) > Mathf.Abs(stopPointX))
            {
                transform.Translate(sideTo * speed * Time.deltaTime);
                if (!cowboys_move_to_center_started)
                {
                    print("MUSIC!!!");
                    cowboys_move_to_center_started = true;
                    SoundManager.instance.playCowboysMoveToCenterMusic();
                }
            }
            else
            {
                cowboys_move_to_center_started = false;
                if (!playerStoped)
                {
                    playerStoped = true;
                    playerStopedController = true;
                    GameStateManager.cowboysReadyForDuel++;
                    animator.Idle();
                } 

                if (GameStateManager.cowboysReadyForDuel == 2)
                {
                    GameStateManager.currentGameState = (int)GameStateManager.DuelStates.cowboys_ready;
                    GameStateManager.cowboysReadyForDuel = 0;   //reset value for next cowboys
                    playerStopedController = false;  //reset for next spawned cowboys 
                }
            }
        }
    }

    void CalculateStopPoints()
    {
        if (transform.position.x < 0)
        {
            //if cowboy on left side
            stopPointX = -pixelsFromCenter;
            sideTo = Vector2.right;
        }
        else
        {
            //if cowboy on right side
            stopPointX = pixelsFromCenter;
            sideTo = Vector2.left;
        }
    }

}
