﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PixelPerfectCamera
struct PixelPerfectCamera_t2668956642;

#include "codegen/il2cpp-codegen.h"

// System.Void PixelPerfectCamera::.ctor()
extern "C"  void PixelPerfectCamera__ctor_m2232840197 (PixelPerfectCamera_t2668956642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PixelPerfectCamera::.cctor()
extern "C"  void PixelPerfectCamera__cctor_m2242190036 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PixelPerfectCamera::Awake()
extern "C"  void PixelPerfectCamera_Awake_m1603331042 (PixelPerfectCamera_t2668956642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
