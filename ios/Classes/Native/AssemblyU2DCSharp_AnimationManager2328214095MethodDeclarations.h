﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimationManager
struct AnimationManager_t2328214095;

#include "codegen/il2cpp-codegen.h"

// System.Void AnimationManager::.ctor()
extern "C"  void AnimationManager__ctor_m1277738448 (AnimationManager_t2328214095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationManager::Awake()
extern "C"  void AnimationManager_Awake_m1911272715 (AnimationManager_t2328214095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationManager::Walk()
extern "C"  void AnimationManager_Walk_m262311495 (AnimationManager_t2328214095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationManager::Idle()
extern "C"  void AnimationManager_Idle_m2360568088 (AnimationManager_t2328214095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationManager::IdleWithGun()
extern "C"  void AnimationManager_IdleWithGun_m2317408456 (AnimationManager_t2328214095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationManager::Shot()
extern "C"  void AnimationManager_Shot_m2722230414 (AnimationManager_t2328214095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationManager::Bang()
extern "C"  void AnimationManager_Bang_m2150847500 (AnimationManager_t2328214095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationManager::Win()
extern "C"  void AnimationManager_Win_m4182162318 (AnimationManager_t2328214095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AnimationManager::GetCurrentAnimationTimeLength()
extern "C"  float AnimationManager_GetCurrentAnimationTimeLength_m3286674554 (AnimationManager_t2328214095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
