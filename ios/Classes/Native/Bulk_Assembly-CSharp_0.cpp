﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// AnimationManager
struct AnimationManager_t2328214095;
// UnityEngine.Animator
struct Animator_t2442207934;
// System.Object
struct Il2CppObject;
// Death
struct Death_t2883266902;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3863917503;
// DuelBeforeShotAnimation
struct DuelBeforeShotAnimation_t4264097629;
// System.Collections.IEnumerator
struct IEnumerator_t3037427797;
// DuelBeforeShotAnimation/<CalculateAnimationLength>c__Iterator0
struct U3CCalculateAnimationLengthU3Ec__Iterator0_t174940195;
// GameStateManager
struct GameStateManager_t1063875770;
// MissShotController
struct MissShotController_t1066454504;
// MissShotController/<waitForBangAnimationEnds>c__Iterator2
struct U3CwaitForBangAnimationEndsU3Ec__Iterator2_t3907694863;
// MissShotController/<WaitForShotCorutine>c__Iterator1
struct U3CWaitForShotCorutineU3Ec__Iterator1_t80400909;
// PixelPerfectCamera
struct PixelPerfectCamera_t2668956642;
// UnityEngine.Camera
struct Camera_t2805735124;
// PrepareToDuel
struct PrepareToDuel_t908926924;
// ReSkinAnimation
struct ReSkinAnimation_t3053752670;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t2914288752;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t4275260569;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3632007997;
// UnityEngine.SpriteRenderer[]
struct SpriteRendererU5BU5D_t1066967654;
// UnityEngine.Sprite
struct Sprite_t1118776648;
// System.Predicate`1<UnityEngine.Sprite>
struct Predicate_1_t231192664;
// System.Predicate`1<System.Object>
struct Predicate_1_t4115352452;
// ReSkinAnimation/<LateUpdate>c__AnonStorey6
struct U3CLateUpdateU3Ec__AnonStorey6_t4292923148;
// Rotator
struct Rotator_t3375904803;
// Shot
struct Shot_t3441902592;
// Shot/<WaitForSpawnNewCowboy>c__Iterator3
struct U3CWaitForSpawnNewCowboyU3Ec__Iterator3_t319929336;
// ShowScore
struct ShowScore_t4012426113;
// SoundManager
struct SoundManager_t654432262;
// UnityEngine.AudioClip
struct AudioClip_t3927647597;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t3948543552;
// Spawner
struct Spawner_t534830648;
// UnityEngine.GameObject
struct GameObject_t1366199518;
// StartGame
struct StartGame_t415325182;
// WinStateCowboyAnimation
struct WinStateCowboyAnimation_t2747849222;
// WinStateCowboyAnimation/<WaitForRestart>c__Iterator5
struct U3CWaitForRestartU3Ec__Iterator5_t2340398903;
// WinStateCowboyAnimation/<WaitForWin>c__Iterator4
struct U3CWaitForWinU3Ec__Iterator4_t1495125349;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array4136897760.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnimationManager2328214095.h"
#include "AssemblyU2DCSharp_AnimationManager2328214095MethodDeclarations.h"
#include "mscorlib_System_Void2799814243.h"
#include "UnityEngine_UnityEngine_MonoBehaviour774292115MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component1078601330MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator2442207934.h"
#include "UnityEngine_UnityEngine_Component1078601330.h"
#include "UnityEngine_UnityEngine_Animator2442207934MethodDeclarations.h"
#include "mscorlib_System_String1967731336.h"
#include "mscorlib_System_Int321448170597.h"
#include "mscorlib_System_Single1791520093.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo3342076411MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo3342076411.h"
#include "AssemblyU2DCSharp_AnimationManager_MyAnimations3023864867.h"
#include "AssemblyU2DCSharp_AnimationManager_MyAnimations3023864867MethodDeclarations.h"
#include "AssemblyU2DCSharp_Death2883266902.h"
#include "AssemblyU2DCSharp_Death2883266902MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1181371020MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1366199518.h"
#include "UnityEngine_UnityEngine_Object1181371020.h"
#include "UnityEngine_UnityEngine_Renderer2715231144MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer3863917503.h"
#include "mscorlib_System_Boolean3143194569.h"
#include "AssemblyU2DCSharp_DuelBeforeShotAnimation4264097629.h"
#include "AssemblyU2DCSharp_DuelBeforeShotAnimation4264097629MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1366199518MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameStateManager1063875770MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input4173266137MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform224878301MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen173559997MethodDeclarations.h"
#include "AssemblyU2DCSharp_SoundManager654432262MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer3863917503MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3465617797.h"
#include "UnityEngine_UnityEngine_Transform224878301.h"
#include "AssemblyU2DCSharp_GameStateManager1063875770.h"
#include "AssemblyU2DCSharp_SoundManager654432262.h"
#include "UnityEngine_UnityEngine_Coroutine3261918659.h"
#include "AssemblyU2DCSharp_DuelBeforeShotAnimation_U3CCalcul174940195MethodDeclarations.h"
#include "AssemblyU2DCSharp_DuelBeforeShotAnimation_U3CCalcul174940195.h"
#include "mscorlib_System_Object707969140MethodDeclarations.h"
#include "mscorlib_System_Object707969140.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame2828800938MethodDeclarations.h"
#include "mscorlib_System_UInt323922122178.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame2828800938.h"
#include "mscorlib_System_NotSupportedException3178859535MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException3178859535.h"
#include "UnityEngine_UnityEngine_Random265084658MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameStateManager_DuelStates3006973384.h"
#include "AssemblyU2DCSharp_GameStateManager_DuelStates3006973384MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameStateManager_WhoToSpawn2156964408.h"
#include "AssemblyU2DCSharp_GameStateManager_WhoToSpawn2156964408MethodDeclarations.h"
#include "AssemblyU2DCSharp_MissShotController1066454504.h"
#include "AssemblyU2DCSharp_MissShotController1066454504MethodDeclarations.h"
#include "AssemblyU2DCSharp_MissShotController_U3CWaitForShotC80400909MethodDeclarations.h"
#include "AssemblyU2DCSharp_MissShotController_U3CWaitForShotC80400909.h"
#include "AssemblyU2DCSharp_MissShotController_U3CwaitForBan3907694863MethodDeclarations.h"
#include "AssemblyU2DCSharp_MissShotController_U3CwaitForBan3907694863.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1717981302MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1717981302.h"
#include "AssemblyU2DCSharp_PixelPerfectCamera2668956642.h"
#include "AssemblyU2DCSharp_PixelPerfectCamera2668956642MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector2465617798MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector2465617798.h"
#include "UnityEngine_UnityEngine_Camera2805735124MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2805735124.h"
#include "AssemblyU2DCSharp_PrepareToDuel908926924.h"
#include "AssemblyU2DCSharp_PrepareToDuel908926924MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time2587606660MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf1692945841MethodDeclarations.h"
#include "AssemblyU2DCSharp_ReSkinAnimation3053752670.h"
#include "AssemblyU2DCSharp_ReSkinAnimation3053752670MethodDeclarations.h"
#include "UnityEngine_UnityEngine_BoxCollider2D2914288752.h"
#include "mscorlib_System_String1967731336MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources3616872916MethodDeclarations.h"
#include "AssemblyU2DCSharp_ReSkinAnimation_U3CLateUpdateU3E4292923148MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen231192664MethodDeclarations.h"
#include "mscorlib_System_Array4136897760MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Sprite1118776648MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds968365060MethodDeclarations.h"
#include "UnityEngine_UnityEngine_BoxCollider2D2914288752MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Sprite1118776648.h"
#include "AssemblyU2DCSharp_ReSkinAnimation_U3CLateUpdateU3E4292923148.h"
#include "UnityEngine_UnityEngine_Bounds968365060.h"
#include "UnityEngine_UnityEngine_Resources3616872916.h"
#include "mscorlib_System_Predicate_1_gen231192664.h"
#include "mscorlib_System_IntPtr3076297692.h"
#include "AssemblyU2DCSharp_Rotator3375904803.h"
#include "AssemblyU2DCSharp_Rotator3375904803MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3465617797MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion83606849MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion83606849.h"
#include "AssemblyU2DCSharp_Shot3441902592.h"
#include "AssemblyU2DCSharp_Shot3441902592MethodDeclarations.h"
#include "AssemblyU2DCSharp_Shot_U3CWaitForSpawnNewCowboyU3Ec319929336MethodDeclarations.h"
#include "AssemblyU2DCSharp_Shot_U3CWaitForSpawnNewCowboyU3Ec319929336.h"
#include "AssemblyU2DCSharp_ShowScore4012426113.h"
#include "AssemblyU2DCSharp_ShowScore4012426113MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3921196294.h"
#include "mscorlib_System_Int321448170597MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3921196294MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip3927647597.h"
#include "UnityEngine_UnityEngine_AudioSource585923902MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource585923902.h"
#include "AssemblyU2DCSharp_Spawner534830648.h"
#include "AssemblyU2DCSharp_Spawner534830648MethodDeclarations.h"
#include "AssemblyU2DCSharp_StartGame415325182.h"
#include "AssemblyU2DCSharp_StartGame415325182MethodDeclarations.h"
#include "AssemblyU2DCSharp_WinStateCowboyAnimation2747849222.h"
#include "AssemblyU2DCSharp_WinStateCowboyAnimation2747849222MethodDeclarations.h"
#include "AssemblyU2DCSharp_WinStateCowboyAnimation_U3CWaitF1495125349MethodDeclarations.h"
#include "AssemblyU2DCSharp_WinStateCowboyAnimation_U3CWaitF1495125349.h"
#include "AssemblyU2DCSharp_WinStateCowboyAnimation_U3CWaitF2340398903MethodDeclarations.h"
#include "AssemblyU2DCSharp_WinStateCowboyAnimation_U3CWaitF2340398903.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m2721246802_gshared (Component_t1078601330 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m2721246802(__this, method) ((  Il2CppObject * (*) (Component_t1078601330 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2721246802_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t2442207934_m475627522(__this, method) ((  Animator_t2442207934 * (*) (Component_t1078601330 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2721246802_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
#define Component_GetComponent_TisSpriteRenderer_t3863917503_m2178781570(__this, method) ((  SpriteRenderer_t3863917503 * (*) (Component_t1078601330 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2721246802_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<AnimationManager>()
#define Component_GetComponent_TisAnimationManager_t2328214095_m400137410(__this, method) ((  AnimationManager_t2328214095 * (*) (Component_t1078601330 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2721246802_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1366199518 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1366199518 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.SpriteRenderer>()
#define GameObject_GetComponent_TisSpriteRenderer_t3863917503_m1184556631(__this, method) ((  SpriteRenderer_t3863917503 * (*) (GameObject_t1366199518 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t2805735124_m3276577584(__this, method) ((  Camera_t2805735124 * (*) (Component_t1078601330 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2721246802_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.BoxCollider2D>()
#define Component_GetComponent_TisBoxCollider2D_t2914288752_m324820273(__this, method) ((  BoxCollider2D_t2914288752 * (*) (Component_t1078601330 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2721246802_gshared)(__this, method)
// !!0[] UnityEngine.Resources::LoadAll<System.Object>(System.String)
extern "C"  ObjectU5BU5D_t3632007997* Resources_LoadAll_TisIl2CppObject_m579936881_gshared (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method);
#define Resources_LoadAll_TisIl2CppObject_m579936881(__this /* static, unused */, p0, method) ((  ObjectU5BU5D_t3632007997* (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_LoadAll_TisIl2CppObject_m579936881_gshared)(__this /* static, unused */, p0, method)
// !!0[] UnityEngine.Resources::LoadAll<UnityEngine.Sprite>(System.String)
#define Resources_LoadAll_TisSprite_t1118776648_m2824615481(__this /* static, unused */, p0, method) ((  SpriteU5BU5D_t4275260569* (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_LoadAll_TisIl2CppObject_m579936881_gshared)(__this /* static, unused */, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t3632007997* Component_GetComponentsInChildren_TisIl2CppObject_m3978412804_gshared (Component_t1078601330 * __this, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisIl2CppObject_m3978412804(__this, method) ((  ObjectU5BU5D_t3632007997* (*) (Component_t1078601330 *, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m3978412804_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.SpriteRenderer>()
#define Component_GetComponentsInChildren_TisSpriteRenderer_t3863917503_m1456864797(__this, method) ((  SpriteRendererU5BU5D_t1066967654* (*) (Component_t1078601330 *, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m3978412804_gshared)(__this, method)
// !!0 System.Array::Find<System.Object>(!!0[],System.Predicate`1<!!0>)
extern "C"  Il2CppObject * Array_Find_TisIl2CppObject_m4028986414_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3632007997* p0, Predicate_1_t4115352452 * p1, const MethodInfo* method);
#define Array_Find_TisIl2CppObject_m4028986414(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3632007997*, Predicate_1_t4115352452 *, const MethodInfo*))Array_Find_TisIl2CppObject_m4028986414_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Array::Find<UnityEngine.Sprite>(!!0[],System.Predicate`1<!!0>)
#define Array_Find_TisSprite_t1118776648_m3404915674(__this /* static, unused */, p0, p1, method) ((  Sprite_t1118776648 * (*) (Il2CppObject * /* static, unused */, SpriteU5BU5D_t4275260569*, Predicate_1_t231192664 *, const MethodInfo*))Array_Find_TisIl2CppObject_m4028986414_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m447919519_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m447919519(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t1366199518_m3664764861(__this /* static, unused */, p0, method) ((  GameObject_t1366199518 * (*) (Il2CppObject * /* static, unused */, GameObject_t1366199518 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<ReSkinAnimation>()
#define GameObject_GetComponent_TisReSkinAnimation_t3053752670_m4260080765(__this, method) ((  ReSkinAnimation_t3053752670 * (*) (GameObject_t1366199518 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AnimationManager::.ctor()
extern "C"  void AnimationManager__ctor_m1277738448 (AnimationManager_t2328214095 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimationManager::Awake()
extern const MethodInfo* Component_GetComponent_TisAnimator_t2442207934_m475627522_MethodInfo_var;
extern const uint32_t AnimationManager_Awake_m1911272715_MetadataUsageId;
extern "C"  void AnimationManager_Awake_m1911272715 (AnimationManager_t2328214095 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationManager_Awake_m1911272715_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2442207934 * L_0 = Component_GetComponent_TisAnimator_t2442207934_m475627522(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t2442207934_m475627522_MethodInfo_var);
		__this->set_animator_2(L_0);
		return;
	}
}
// System.Void AnimationManager::Walk()
extern Il2CppCodeGenString* _stringLiteral1695925803;
extern const uint32_t AnimationManager_Walk_m262311495_MetadataUsageId;
extern "C"  void AnimationManager_Walk_m262311495 (AnimationManager_t2328214095 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationManager_Walk_m262311495_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2442207934 * L_0 = __this->get_animator_2();
		NullCheck(L_0);
		Animator_SetInteger_m528582597(L_0, _stringLiteral1695925803, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimationManager::Idle()
extern Il2CppCodeGenString* _stringLiteral1695925803;
extern const uint32_t AnimationManager_Idle_m2360568088_MetadataUsageId;
extern "C"  void AnimationManager_Idle_m2360568088 (AnimationManager_t2328214095 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationManager_Idle_m2360568088_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2442207934 * L_0 = __this->get_animator_2();
		NullCheck(L_0);
		Animator_SetInteger_m528582597(L_0, _stringLiteral1695925803, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimationManager::IdleWithGun()
extern Il2CppCodeGenString* _stringLiteral1695925803;
extern const uint32_t AnimationManager_IdleWithGun_m2317408456_MetadataUsageId;
extern "C"  void AnimationManager_IdleWithGun_m2317408456 (AnimationManager_t2328214095 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationManager_IdleWithGun_m2317408456_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2442207934 * L_0 = __this->get_animator_2();
		NullCheck(L_0);
		Animator_SetInteger_m528582597(L_0, _stringLiteral1695925803, 2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimationManager::Shot()
extern Il2CppCodeGenString* _stringLiteral1695925803;
extern const uint32_t AnimationManager_Shot_m2722230414_MetadataUsageId;
extern "C"  void AnimationManager_Shot_m2722230414 (AnimationManager_t2328214095 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationManager_Shot_m2722230414_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2442207934 * L_0 = __this->get_animator_2();
		NullCheck(L_0);
		Animator_SetInteger_m528582597(L_0, _stringLiteral1695925803, 3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimationManager::Bang()
extern Il2CppCodeGenString* _stringLiteral1695925803;
extern const uint32_t AnimationManager_Bang_m2150847500_MetadataUsageId;
extern "C"  void AnimationManager_Bang_m2150847500 (AnimationManager_t2328214095 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationManager_Bang_m2150847500_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2442207934 * L_0 = __this->get_animator_2();
		NullCheck(L_0);
		Animator_SetInteger_m528582597(L_0, _stringLiteral1695925803, 4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimationManager::Win()
extern Il2CppCodeGenString* _stringLiteral1695925803;
extern const uint32_t AnimationManager_Win_m4182162318_MetadataUsageId;
extern "C"  void AnimationManager_Win_m4182162318 (AnimationManager_t2328214095 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationManager_Win_m4182162318_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t2442207934 * L_0 = __this->get_animator_2();
		NullCheck(L_0);
		Animator_SetInteger_m528582597(L_0, _stringLiteral1695925803, 5, /*hidden argument*/NULL);
		return;
	}
}
// System.Single AnimationManager::GetCurrentAnimationTimeLength()
extern "C"  float AnimationManager_GetCurrentAnimationTimeLength_m3286674554 (AnimationManager_t2328214095 * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t3342076411  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_t2442207934 * L_0 = __this->get_animator_2();
		NullCheck(L_0);
		AnimatorStateInfo_t3342076411  L_1 = Animator_GetCurrentAnimatorStateInfo_m1931338898(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = AnimatorStateInfo_get_length_m3151009408((&V_0), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Death::.ctor()
extern "C"  void Death__ctor_m2539501311 (Death_t2883266902 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Death::Die()
extern "C"  void Death_Die_m166476791 (Death_t2883266902 * __this, const MethodInfo* method)
{
	{
		GameObject_t1366199518 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Death::Disable()
extern const MethodInfo* Component_GetComponent_TisSpriteRenderer_t3863917503_m2178781570_MethodInfo_var;
extern const uint32_t Death_Disable_m287603695_MetadataUsageId;
extern "C"  void Death_Disable_m287603695 (Death_t2883266902 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Death_Disable_m287603695_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SpriteRenderer_t3863917503 * L_0 = Component_GetComponent_TisSpriteRenderer_t3863917503_m2178781570(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3863917503_m2178781570_MethodInfo_var);
		NullCheck(L_0);
		Renderer_set_enabled_m142717579(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DuelBeforeShotAnimation::.ctor()
extern "C"  void DuelBeforeShotAnimation__ctor_m1957131436 (DuelBeforeShotAnimation_t4264097629 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DuelBeforeShotAnimation::.cctor()
extern "C"  void DuelBeforeShotAnimation__cctor_m768009431 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DuelBeforeShotAnimation::Start()
extern const MethodInfo* Component_GetComponent_TisAnimationManager_t2328214095_m400137410_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1794533022;
extern const uint32_t DuelBeforeShotAnimation_Start_m2208841316_MetadataUsageId;
extern "C"  void DuelBeforeShotAnimation_Start_m2208841316 (DuelBeforeShotAnimation_t4264097629 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DuelBeforeShotAnimation_Start_m2208841316_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AnimationManager_t2328214095 * L_0 = Component_GetComponent_TisAnimationManager_t2328214095_m400137410(__this, /*hidden argument*/Component_GetComponent_TisAnimationManager_t2328214095_m400137410_MethodInfo_var);
		__this->set_animator_3(L_0);
		GameObject_t1366199518 * L_1 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral1794533022, /*hidden argument*/NULL);
		__this->set_missShotController_6(L_1);
		return;
	}
}
// System.Void DuelBeforeShotAnimation::Update()
extern TypeInfo* GameStateManager_t1063875770_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t4173266137_il2cpp_TypeInfo_var;
extern TypeInfo* DuelBeforeShotAnimation_t4264097629_il2cpp_TypeInfo_var;
extern TypeInfo* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t3863917503_m1184556631_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1973084735;
extern Il2CppCodeGenString* _stringLiteral1662431292;
extern const uint32_t DuelBeforeShotAnimation_Update_m2108688449_MetadataUsageId;
extern "C"  void DuelBeforeShotAnimation_Update_m2108688449 (DuelBeforeShotAnimation_t4264097629 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DuelBeforeShotAnimation_Update_m2108688449_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SpriteRenderer_t3863917503 * V_0 = NULL;
	Vector3_t465617797  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t465617797  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t465617797  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t465617797  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		GameObject_t1366199518 * L_0 = __this->get_playerRight_8();
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1181371020 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		GameObject_t1366199518 * L_2 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral1973084735, /*hidden argument*/NULL);
		__this->set_playerRight_8(L_2);
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_3 = GameStateManager_get_currentGameState_m2409036446(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)3))))
		{
			goto IL_013d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4173266137_il2cpp_TypeInfo_var);
		bool L_4 = Input_GetMouseButtonDown_m47917805(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_013d;
		}
	}
	{
		Transform_t224878301 * L_5 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t465617797  L_6 = Transform_get_position_m1104419803(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = (&V_1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4173266137_il2cpp_TypeInfo_var);
		Vector3_t465617797  L_8 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_8;
		float L_9 = (&V_2)->get_x_1();
		int32_t L_10 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DuelBeforeShotAnimation_t4264097629_il2cpp_TypeInfo_var);
		bool L_11 = ((DuelBeforeShotAnimation_t4264097629_StaticFields*)DuelBeforeShotAnimation_t4264097629_il2cpp_TypeInfo_var->static_fields)->get_suicide_9();
		if (!((int32_t)((int32_t)((int32_t)((int32_t)((((float)L_7) < ((float)(0.0f)))? 1 : 0)&(int32_t)((((float)L_9) < ((float)(((float)((float)((int32_t)((int32_t)L_10/(int32_t)2)))))))? 1 : 0)))&(int32_t)((((int32_t)L_11) == ((int32_t)0))? 1 : 0))))
		{
			goto IL_00b0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DuelBeforeShotAnimation_t4264097629_il2cpp_TypeInfo_var);
		((DuelBeforeShotAnimation_t4264097629_StaticFields*)DuelBeforeShotAnimation_t4264097629_il2cpp_TypeInfo_var->static_fields)->set_suicide_9((bool)1);
		AnimationManager_t2328214095 * L_12 = __this->get_animator_3();
		NullCheck(L_12);
		AnimationManager_Bang_m2150847500(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->set_needToSpawn_7(0);
		int32_t L_13 = ((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get_scoreRight_6();
		((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->set_scoreRight_6(((int32_t)((int32_t)L_13+(int32_t)1)));
		IL2CPP_RUNTIME_CLASS_INIT(SoundManager_t654432262_il2cpp_TypeInfo_var);
		SoundManager_t654432262 * L_14 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_3();
		NullCheck(L_14);
		SoundManager_PlaySuicideSound_m707867868(L_14, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m1399371129(__this, _stringLiteral1662431292, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		Transform_t224878301 * L_15 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t465617797  L_16 = Transform_get_position_m1104419803(L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		float L_17 = (&V_3)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4173266137_il2cpp_TypeInfo_var);
		Vector3_t465617797  L_18 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_18;
		float L_19 = (&V_4)->get_x_1();
		int32_t L_20 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DuelBeforeShotAnimation_t4264097629_il2cpp_TypeInfo_var);
		bool L_21 = ((DuelBeforeShotAnimation_t4264097629_StaticFields*)DuelBeforeShotAnimation_t4264097629_il2cpp_TypeInfo_var->static_fields)->get_suicide_9();
		if (!((int32_t)((int32_t)((int32_t)((int32_t)((((float)L_17) > ((float)(0.0f)))? 1 : 0)&(int32_t)((((float)L_19) > ((float)(((float)((float)((int32_t)((int32_t)L_20/(int32_t)2)))))))? 1 : 0)))&(int32_t)((((int32_t)L_21) == ((int32_t)0))? 1 : 0))))
		{
			goto IL_013d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DuelBeforeShotAnimation_t4264097629_il2cpp_TypeInfo_var);
		((DuelBeforeShotAnimation_t4264097629_StaticFields*)DuelBeforeShotAnimation_t4264097629_il2cpp_TypeInfo_var->static_fields)->set_suicide_9((bool)1);
		GameObject_t1366199518 * L_22 = __this->get_playerRight_8();
		NullCheck(L_22);
		SpriteRenderer_t3863917503 * L_23 = GameObject_GetComponent_TisSpriteRenderer_t3863917503_m1184556631(L_22, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3863917503_m1184556631_MethodInfo_var);
		V_0 = L_23;
		SpriteRenderer_t3863917503 * L_24 = V_0;
		NullCheck(L_24);
		SpriteRenderer_set_flipX_m994174828(L_24, (bool)0, /*hidden argument*/NULL);
		AnimationManager_t2328214095 * L_25 = __this->get_animator_3();
		NullCheck(L_25);
		AnimationManager_Bang_m2150847500(L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->set_needToSpawn_7(1);
		int32_t L_26 = ((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get_scoreLeft_5();
		((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->set_scoreLeft_5(((int32_t)((int32_t)L_26+(int32_t)1)));
		IL2CPP_RUNTIME_CLASS_INIT(SoundManager_t654432262_il2cpp_TypeInfo_var);
		SoundManager_t654432262 * L_27 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_3();
		NullCheck(L_27);
		SoundManager_PlaySuicideSound_m707867868(L_27, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m1399371129(__this, _stringLiteral1662431292, /*hidden argument*/NULL);
	}

IL_013d:
	{
		return;
	}
}
// System.Collections.IEnumerator DuelBeforeShotAnimation::CalculateAnimationLength()
extern TypeInfo* U3CCalculateAnimationLengthU3Ec__Iterator0_t174940195_il2cpp_TypeInfo_var;
extern const uint32_t DuelBeforeShotAnimation_CalculateAnimationLength_m1743222608_MetadataUsageId;
extern "C"  Il2CppObject * DuelBeforeShotAnimation_CalculateAnimationLength_m1743222608 (DuelBeforeShotAnimation_t4264097629 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DuelBeforeShotAnimation_CalculateAnimationLength_m1743222608_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CCalculateAnimationLengthU3Ec__Iterator0_t174940195 * V_0 = NULL;
	{
		U3CCalculateAnimationLengthU3Ec__Iterator0_t174940195 * L_0 = (U3CCalculateAnimationLengthU3Ec__Iterator0_t174940195 *)il2cpp_codegen_object_new(U3CCalculateAnimationLengthU3Ec__Iterator0_t174940195_il2cpp_TypeInfo_var);
		U3CCalculateAnimationLengthU3Ec__Iterator0__ctor_m1257055024(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCalculateAnimationLengthU3Ec__Iterator0_t174940195 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CCalculateAnimationLengthU3Ec__Iterator0_t174940195 * L_2 = V_0;
		return L_2;
	}
}
// System.Void DuelBeforeShotAnimation/<CalculateAnimationLength>c__Iterator0::.ctor()
extern "C"  void U3CCalculateAnimationLengthU3Ec__Iterator0__ctor_m1257055024 (U3CCalculateAnimationLengthU3Ec__Iterator0_t174940195 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object DuelBeforeShotAnimation/<CalculateAnimationLength>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCalculateAnimationLengthU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m597024660 (U3CCalculateAnimationLengthU3Ec__Iterator0_t174940195 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object DuelBeforeShotAnimation/<CalculateAnimationLength>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCalculateAnimationLengthU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m871187340 (U3CCalculateAnimationLengthU3Ec__Iterator0_t174940195 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean DuelBeforeShotAnimation/<CalculateAnimationLength>c__Iterator0::MoveNext()
extern TypeInfo* WaitForEndOfFrame_t2828800938_il2cpp_TypeInfo_var;
extern TypeInfo* DuelBeforeShotAnimation_t4264097629_il2cpp_TypeInfo_var;
extern TypeInfo* GameStateManager_t1063875770_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral103841973;
extern const uint32_t U3CCalculateAnimationLengthU3Ec__Iterator0_MoveNext_m2947384468_MetadataUsageId;
extern "C"  bool U3CCalculateAnimationLengthU3Ec__Iterator0_MoveNext_m2947384468 (U3CCalculateAnimationLengthU3Ec__Iterator0_t174940195 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCalculateAnimationLengthU3Ec__Iterator0_MoveNext_m2947384468_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0038;
		}
	}
	{
		goto IL_008d;
	}

IL_0021:
	{
		WaitForEndOfFrame_t2828800938 * L_2 = (WaitForEndOfFrame_t2828800938 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t2828800938_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m3062480170(L_2, /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		__this->set_U24PC_0(1);
		goto IL_008f;
	}

IL_0038:
	{
		DuelBeforeShotAnimation_t4264097629 * L_3 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_3);
		AnimationManager_t2328214095 * L_4 = L_3->get_animator_3();
		NullCheck(L_4);
		float L_5 = AnimationManager_GetCurrentAnimationTimeLength_m3286674554(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DuelBeforeShotAnimation_t4264097629_il2cpp_TypeInfo_var);
		((DuelBeforeShotAnimation_t4264097629_StaticFields*)DuelBeforeShotAnimation_t4264097629_il2cpp_TypeInfo_var->static_fields)->set_bangAnimationLength_4(L_5);
		DuelBeforeShotAnimation_t4264097629 * L_6 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_6);
		GameObject_t1366199518 * L_7 = L_6->get_missShotController_6();
		NullCheck(L_7);
		GameObject_SendMessage_m1177535567(L_7, _stringLiteral103841973, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_8 = ((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get_scoreLeft_5();
		int32_t L_9 = ((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get_maxScore_2();
		if ((((int32_t)L_8) == ((int32_t)L_9)))
		{
			goto IL_0080;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_10 = ((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get_scoreRight_6();
		int32_t L_11 = ((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get_maxScore_2();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_0086;
		}
	}

IL_0080:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		GameStateManager_set_currentGameState_m1712983881(NULL /*static, unused*/, 6, /*hidden argument*/NULL);
	}

IL_0086:
	{
		__this->set_U24PC_0((-1));
	}

IL_008d:
	{
		return (bool)0;
	}

IL_008f:
	{
		return (bool)1;
	}
	// Dead block : IL_0091: ldloc.1
}
// System.Void DuelBeforeShotAnimation/<CalculateAnimationLength>c__Iterator0::Dispose()
extern "C"  void U3CCalculateAnimationLengthU3Ec__Iterator0_Dispose_m2752935565 (U3CCalculateAnimationLengthU3Ec__Iterator0_t174940195 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void DuelBeforeShotAnimation/<CalculateAnimationLength>c__Iterator0::Reset()
extern TypeInfo* NotSupportedException_t3178859535_il2cpp_TypeInfo_var;
extern const uint32_t U3CCalculateAnimationLengthU3Ec__Iterator0_Reset_m424804167_MetadataUsageId;
extern "C"  void U3CCalculateAnimationLengthU3Ec__Iterator0_Reset_m424804167 (U3CCalculateAnimationLengthU3Ec__Iterator0_t174940195 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCalculateAnimationLengthU3Ec__Iterator0_Reset_m424804167_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t3178859535 * L_0 = (NotSupportedException_t3178859535 *)il2cpp_codegen_object_new(NotSupportedException_t3178859535_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void GameStateManager::.ctor()
extern "C"  void GameStateManager__ctor_m189335085 (GameStateManager_t1063875770 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameStateManager::.cctor()
extern TypeInfo* GameStateManager_t1063875770_il2cpp_TypeInfo_var;
extern const uint32_t GameStateManager__cctor_m623853036_MetadataUsageId;
extern "C"  void GameStateManager__cctor_m623853036 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameStateManager__cctor_m623853036_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->set_maxScore_2(5);
		((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->set_needToSpawn_7(2);
		return;
	}
}
// System.Int32 GameStateManager::get_currentGameState()
extern TypeInfo* GameStateManager_t1063875770_il2cpp_TypeInfo_var;
extern const uint32_t GameStateManager_get_currentGameState_m2409036446_MetadataUsageId;
extern "C"  int32_t GameStateManager_get_currentGameState_m2409036446 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameStateManager_get_currentGameState_m2409036446_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_0 = ((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get__currentGameState_3();
		return L_0;
	}
}
// System.Void GameStateManager::set_currentGameState(System.Int32)
extern TypeInfo* GameStateManager_t1063875770_il2cpp_TypeInfo_var;
extern const uint32_t GameStateManager_set_currentGameState_m1712983881_MetadataUsageId;
extern "C"  void GameStateManager_set_currentGameState_m1712983881 (Il2CppObject * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameStateManager_set_currentGameState_m1712983881_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->set__currentGameState_3(L_0);
		return;
	}
}
// System.Int32 GameStateManager::get_RandomDelay()
extern TypeInfo* GameStateManager_t1063875770_il2cpp_TypeInfo_var;
extern const uint32_t GameStateManager_get_RandomDelay_m3340467374_MetadataUsageId;
extern "C"  int32_t GameStateManager_get_RandomDelay_m3340467374 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameStateManager_get_RandomDelay_m3340467374_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_0 = ((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get__randomDelay_8();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = Random_Range_m694320887(NULL /*static, unused*/, 3, ((int32_t)10), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->set__randomDelay_8(L_1);
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_2 = ((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get__randomDelay_8();
		return L_2;
	}
}
// System.Void GameStateManager::set_RandomDelay(System.Int32)
extern TypeInfo* GameStateManager_t1063875770_il2cpp_TypeInfo_var;
extern const uint32_t GameStateManager_set_RandomDelay_m475923579_MetadataUsageId;
extern "C"  void GameStateManager_set_RandomDelay_m475923579 (Il2CppObject * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameStateManager_set_RandomDelay_m475923579_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->set__randomDelay_8(L_0);
		return;
	}
}
// System.Int32 GameStateManager::get_CowboysOnScene()
extern Il2CppCodeGenString* _stringLiteral3820598586;
extern Il2CppCodeGenString* _stringLiteral1973084735;
extern const uint32_t GameStateManager_get_CowboysOnScene_m3965318281_MetadataUsageId;
extern "C"  int32_t GameStateManager_get_CowboysOnScene_m3965318281 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameStateManager_get_CowboysOnScene_m3965318281_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		GameObject_t1366199518 * L_0 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral3820598586, /*hidden argument*/NULL);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_2 = V_0;
		V_0 = ((int32_t)((int32_t)L_2+(int32_t)1));
	}

IL_001a:
	{
		GameObject_t1366199518 * L_3 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral1973084735, /*hidden argument*/NULL);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_6 = V_0;
		return L_6;
	}
}
// System.Void GameStateManager::ResetRandomDelay()
extern TypeInfo* GameStateManager_t1063875770_il2cpp_TypeInfo_var;
extern const uint32_t GameStateManager_ResetRandomDelay_m2974787186_MetadataUsageId;
extern "C"  void GameStateManager_ResetRandomDelay_m2974787186 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameStateManager_ResetRandomDelay_m2974787186_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->set__randomDelay_8(0);
		return;
	}
}
// System.Void MissShotController::.ctor()
extern "C"  void MissShotController__ctor_m3541477155 (MissShotController_t1066454504 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MissShotController::.cctor()
extern "C"  void MissShotController__cctor_m2397078434 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MissShotController::Update()
extern TypeInfo* GameStateManager_t1063875770_il2cpp_TypeInfo_var;
extern TypeInfo* MissShotController_t1066454504_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3820598586;
extern Il2CppCodeGenString* _stringLiteral1973084735;
extern Il2CppCodeGenString* _stringLiteral3025662797;
extern const uint32_t MissShotController_Update_m890413674_MetadataUsageId;
extern "C"  void MissShotController_Update_m890413674 (MissShotController_t1066454504 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MissShotController_Update_m890413674_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t1366199518 * L_0 = __this->get_playerLeft_3();
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1181371020 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		GameObject_t1366199518 * L_2 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral3820598586, /*hidden argument*/NULL);
		__this->set_playerLeft_3(L_2);
	}

IL_0021:
	{
		GameObject_t1366199518 * L_3 = __this->get_playerRight_4();
		bool L_4 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_3, (Object_t1181371020 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0042;
		}
	}
	{
		GameObject_t1366199518 * L_5 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral1973084735, /*hidden argument*/NULL);
		__this->set_playerRight_4(L_5);
	}

IL_0042:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_6 = GameStateManager_get_currentGameState_m2409036446(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)3))))
		{
			goto IL_0069;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MissShotController_t1066454504_il2cpp_TypeInfo_var);
		bool L_7 = ((MissShotController_t1066454504_StaticFields*)MissShotController_t1066454504_il2cpp_TypeInfo_var->static_fields)->get_waitForShotStarted_2();
		if (L_7)
		{
			goto IL_0069;
		}
	}
	{
		MonoBehaviour_StartCoroutine_m1399371129(__this, _stringLiteral3025662797, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MissShotController_t1066454504_il2cpp_TypeInfo_var);
		((MissShotController_t1066454504_StaticFields*)MissShotController_t1066454504_il2cpp_TypeInfo_var->static_fields)->set_waitForShotStarted_2((bool)1);
	}

IL_0069:
	{
		return;
	}
}
// System.Collections.IEnumerator MissShotController::WaitForShotCorutine()
extern TypeInfo* U3CWaitForShotCorutineU3Ec__Iterator1_t80400909_il2cpp_TypeInfo_var;
extern const uint32_t MissShotController_WaitForShotCorutine_m378600622_MetadataUsageId;
extern "C"  Il2CppObject * MissShotController_WaitForShotCorutine_m378600622 (MissShotController_t1066454504 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MissShotController_WaitForShotCorutine_m378600622_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CWaitForShotCorutineU3Ec__Iterator1_t80400909 * V_0 = NULL;
	{
		U3CWaitForShotCorutineU3Ec__Iterator1_t80400909 * L_0 = (U3CWaitForShotCorutineU3Ec__Iterator1_t80400909 *)il2cpp_codegen_object_new(U3CWaitForShotCorutineU3Ec__Iterator1_t80400909_il2cpp_TypeInfo_var);
		U3CWaitForShotCorutineU3Ec__Iterator1__ctor_m3051622455(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitForShotCorutineU3Ec__Iterator1_t80400909 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CWaitForShotCorutineU3Ec__Iterator1_t80400909 * L_2 = V_0;
		return L_2;
	}
}
// System.Void MissShotController::StopWaitForShotCorutine()
extern Il2CppCodeGenString* _stringLiteral3025662797;
extern Il2CppCodeGenString* _stringLiteral2934358128;
extern const uint32_t MissShotController_StopWaitForShotCorutine_m665378014_MetadataUsageId;
extern "C"  void MissShotController_StopWaitForShotCorutine_m665378014 (MissShotController_t1066454504 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MissShotController_StopWaitForShotCorutine_m665378014_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_StopCoroutine_m987450539(__this, _stringLiteral3025662797, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m1399371129(__this, _stringLiteral2934358128, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator MissShotController::waitForBangAnimationEnds()
extern TypeInfo* U3CwaitForBangAnimationEndsU3Ec__Iterator2_t3907694863_il2cpp_TypeInfo_var;
extern const uint32_t MissShotController_waitForBangAnimationEnds_m1798326465_MetadataUsageId;
extern "C"  Il2CppObject * MissShotController_waitForBangAnimationEnds_m1798326465 (MissShotController_t1066454504 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MissShotController_waitForBangAnimationEnds_m1798326465_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CwaitForBangAnimationEndsU3Ec__Iterator2_t3907694863 * V_0 = NULL;
	{
		U3CwaitForBangAnimationEndsU3Ec__Iterator2_t3907694863 * L_0 = (U3CwaitForBangAnimationEndsU3Ec__Iterator2_t3907694863 *)il2cpp_codegen_object_new(U3CwaitForBangAnimationEndsU3Ec__Iterator2_t3907694863_il2cpp_TypeInfo_var);
		U3CwaitForBangAnimationEndsU3Ec__Iterator2__ctor_m3129254197(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CwaitForBangAnimationEndsU3Ec__Iterator2_t3907694863 * L_1 = V_0;
		return L_1;
	}
}
// System.Void MissShotController/<waitForBangAnimationEnds>c__Iterator2::.ctor()
extern "C"  void U3CwaitForBangAnimationEndsU3Ec__Iterator2__ctor_m3129254197 (U3CwaitForBangAnimationEndsU3Ec__Iterator2_t3907694863 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object MissShotController/<waitForBangAnimationEnds>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CwaitForBangAnimationEndsU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2231319611 (U3CwaitForBangAnimationEndsU3Ec__Iterator2_t3907694863 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object MissShotController/<waitForBangAnimationEnds>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CwaitForBangAnimationEndsU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2526608723 (U3CwaitForBangAnimationEndsU3Ec__Iterator2_t3907694863 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean MissShotController/<waitForBangAnimationEnds>c__Iterator2::MoveNext()
extern TypeInfo* DuelBeforeShotAnimation_t4264097629_il2cpp_TypeInfo_var;
extern TypeInfo* WaitForSeconds_t1717981302_il2cpp_TypeInfo_var;
extern TypeInfo* GameStateManager_t1063875770_il2cpp_TypeInfo_var;
extern TypeInfo* MissShotController_t1066454504_il2cpp_TypeInfo_var;
extern const uint32_t U3CwaitForBangAnimationEndsU3Ec__Iterator2_MoveNext_m2590649983_MetadataUsageId;
extern "C"  bool U3CwaitForBangAnimationEndsU3Ec__Iterator2_MoveNext_m2590649983 (U3CwaitForBangAnimationEndsU3Ec__Iterator2_t3907694863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CwaitForBangAnimationEndsU3Ec__Iterator2_MoveNext_m2590649983_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003d;
		}
	}
	{
		goto IL_0061;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DuelBeforeShotAnimation_t4264097629_il2cpp_TypeInfo_var);
		float L_2 = ((DuelBeforeShotAnimation_t4264097629_StaticFields*)DuelBeforeShotAnimation_t4264097629_il2cpp_TypeInfo_var->static_fields)->get_bangAnimationLength_4();
		WaitForSeconds_t1717981302 * L_3 = (WaitForSeconds_t1717981302 *)il2cpp_codegen_object_new(WaitForSeconds_t1717981302_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U24current_1(L_3);
		__this->set_U24PC_0(1);
		goto IL_0063;
	}

IL_003d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_4 = GameStateManager_get_currentGameState_m2409036446(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)6)))
		{
			goto IL_004e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		GameStateManager_set_currentGameState_m1712983881(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
	}

IL_004e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DuelBeforeShotAnimation_t4264097629_il2cpp_TypeInfo_var);
		((DuelBeforeShotAnimation_t4264097629_StaticFields*)DuelBeforeShotAnimation_t4264097629_il2cpp_TypeInfo_var->static_fields)->set_suicide_9((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(MissShotController_t1066454504_il2cpp_TypeInfo_var);
		((MissShotController_t1066454504_StaticFields*)MissShotController_t1066454504_il2cpp_TypeInfo_var->static_fields)->set_waitForShotStarted_2((bool)0);
		__this->set_U24PC_0((-1));
	}

IL_0061:
	{
		return (bool)0;
	}

IL_0063:
	{
		return (bool)1;
	}
	// Dead block : IL_0065: ldloc.1
}
// System.Void MissShotController/<waitForBangAnimationEnds>c__Iterator2::Dispose()
extern "C"  void U3CwaitForBangAnimationEndsU3Ec__Iterator2_Dispose_m3395930938 (U3CwaitForBangAnimationEndsU3Ec__Iterator2_t3907694863 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void MissShotController/<waitForBangAnimationEnds>c__Iterator2::Reset()
extern TypeInfo* NotSupportedException_t3178859535_il2cpp_TypeInfo_var;
extern const uint32_t U3CwaitForBangAnimationEndsU3Ec__Iterator2_Reset_m2012661448_MetadataUsageId;
extern "C"  void U3CwaitForBangAnimationEndsU3Ec__Iterator2_Reset_m2012661448 (U3CwaitForBangAnimationEndsU3Ec__Iterator2_t3907694863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CwaitForBangAnimationEndsU3Ec__Iterator2_Reset_m2012661448_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t3178859535 * L_0 = (NotSupportedException_t3178859535 *)il2cpp_codegen_object_new(NotSupportedException_t3178859535_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MissShotController/<WaitForShotCorutine>c__Iterator1::.ctor()
extern "C"  void U3CWaitForShotCorutineU3Ec__Iterator1__ctor_m3051622455 (U3CWaitForShotCorutineU3Ec__Iterator1_t80400909 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object MissShotController/<WaitForShotCorutine>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForShotCorutineU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2201498381 (U3CWaitForShotCorutineU3Ec__Iterator1_t80400909 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object MissShotController/<WaitForShotCorutine>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForShotCorutineU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m774118165 (U3CWaitForShotCorutineU3Ec__Iterator1_t80400909 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean MissShotController/<WaitForShotCorutine>c__Iterator1::MoveNext()
extern TypeInfo* GameStateManager_t1063875770_il2cpp_TypeInfo_var;
extern TypeInfo* WaitForSeconds_t1717981302_il2cpp_TypeInfo_var;
extern TypeInfo* MissShotController_t1066454504_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1788504670;
extern Il2CppCodeGenString* _stringLiteral1840747482;
extern Il2CppCodeGenString* _stringLiteral3398605882;
extern const uint32_t U3CWaitForShotCorutineU3Ec__Iterator1_MoveNext_m1566210489_MetadataUsageId;
extern "C"  bool U3CWaitForShotCorutineU3Ec__Iterator1_MoveNext_m1566210489 (U3CWaitForShotCorutineU3Ec__Iterator1_t80400909 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitForShotCorutineU3Ec__Iterator1_MoveNext_m1566210489_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0048;
		}
	}
	{
		goto IL_009a;
	}

IL_0021:
	{
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, _stringLiteral1788504670, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_2 = GameStateManager_get_RandomDelay_m3340467374(NULL /*static, unused*/, /*hidden argument*/NULL);
		WaitForSeconds_t1717981302 * L_3 = (WaitForSeconds_t1717981302 *)il2cpp_codegen_object_new(WaitForSeconds_t1717981302_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_3, (((float)((float)L_2))), /*hidden argument*/NULL);
		__this->set_U24current_1(L_3);
		__this->set_U24PC_0(1);
		goto IL_009c;
	}

IL_0048:
	{
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, _stringLiteral1840747482, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_4 = GameStateManager_get_CowboysOnScene_m3965318281(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)2))))
		{
			goto IL_008d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		GameStateManager_set_currentGameState_m1712983881(NULL /*static, unused*/, 4, /*hidden argument*/NULL);
		MissShotController_t1066454504 * L_5 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_5);
		GameObject_t1366199518 * L_6 = L_5->get_playerLeft_3();
		NullCheck(L_6);
		GameObject_SendMessage_m1177535567(L_6, _stringLiteral3398605882, /*hidden argument*/NULL);
		MissShotController_t1066454504 * L_7 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_7);
		GameObject_t1366199518 * L_8 = L_7->get_playerRight_4();
		NullCheck(L_8);
		GameObject_SendMessage_m1177535567(L_8, _stringLiteral3398605882, /*hidden argument*/NULL);
	}

IL_008d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MissShotController_t1066454504_il2cpp_TypeInfo_var);
		((MissShotController_t1066454504_StaticFields*)MissShotController_t1066454504_il2cpp_TypeInfo_var->static_fields)->set_waitForShotStarted_2((bool)0);
		__this->set_U24PC_0((-1));
	}

IL_009a:
	{
		return (bool)0;
	}

IL_009c:
	{
		return (bool)1;
	}
	// Dead block : IL_009e: ldloc.1
}
// System.Void MissShotController/<WaitForShotCorutine>c__Iterator1::Dispose()
extern "C"  void U3CWaitForShotCorutineU3Ec__Iterator1_Dispose_m3097202252 (U3CWaitForShotCorutineU3Ec__Iterator1_t80400909 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void MissShotController/<WaitForShotCorutine>c__Iterator1::Reset()
extern TypeInfo* NotSupportedException_t3178859535_il2cpp_TypeInfo_var;
extern const uint32_t U3CWaitForShotCorutineU3Ec__Iterator1_Reset_m440723094_MetadataUsageId;
extern "C"  void U3CWaitForShotCorutineU3Ec__Iterator1_Reset_m440723094 (U3CWaitForShotCorutineU3Ec__Iterator1_t80400909 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitForShotCorutineU3Ec__Iterator1_Reset_m440723094_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t3178859535 * L_0 = (NotSupportedException_t3178859535 *)il2cpp_codegen_object_new(NotSupportedException_t3178859535_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PixelPerfectCamera::.ctor()
extern "C"  void PixelPerfectCamera__ctor_m2232840197 (PixelPerfectCamera_t2668956642 * __this, const MethodInfo* method)
{
	{
		Vector2_t465617798  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m3067419446(&L_0, (320.0f), (160.0f), /*hidden argument*/NULL);
		__this->set_nativeResolution_4(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PixelPerfectCamera::.cctor()
extern TypeInfo* PixelPerfectCamera_t2668956642_il2cpp_TypeInfo_var;
extern const uint32_t PixelPerfectCamera__cctor_m2242190036_MetadataUsageId;
extern "C"  void PixelPerfectCamera__cctor_m2242190036 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PixelPerfectCamera__cctor_m2242190036_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((PixelPerfectCamera_t2668956642_StaticFields*)PixelPerfectCamera_t2668956642_il2cpp_TypeInfo_var->static_fields)->set_pixelsToUnits_2((1.0f));
		((PixelPerfectCamera_t2668956642_StaticFields*)PixelPerfectCamera_t2668956642_il2cpp_TypeInfo_var->static_fields)->set_scale_3((1.0f));
		return;
	}
}
// System.Void PixelPerfectCamera::Awake()
extern TypeInfo* PixelPerfectCamera_t2668956642_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t2805735124_m3276577584_MethodInfo_var;
extern const uint32_t PixelPerfectCamera_Awake_m1603331042_MetadataUsageId;
extern "C"  void PixelPerfectCamera_Awake_m1603331042 (PixelPerfectCamera_t2668956642 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PixelPerfectCamera_Awake_m1603331042_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Camera_t2805735124 * V_0 = NULL;
	{
		Camera_t2805735124 * L_0 = Component_GetComponent_TisCamera_t2805735124_m3276577584(__this, /*hidden argument*/Component_GetComponent_TisCamera_t2805735124_m3276577584_MethodInfo_var);
		V_0 = L_0;
		Camera_t2805735124 * L_1 = V_0;
		NullCheck(L_1);
		bool L_2 = Camera_get_orthographic_m4205491841(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_3 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t465617798 * L_4 = __this->get_address_of_nativeResolution_4();
		float L_5 = L_4->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(PixelPerfectCamera_t2668956642_il2cpp_TypeInfo_var);
		((PixelPerfectCamera_t2668956642_StaticFields*)PixelPerfectCamera_t2668956642_il2cpp_TypeInfo_var->static_fields)->set_scale_3(((float)((float)(((float)((float)L_3)))/(float)L_5)));
		float L_6 = ((PixelPerfectCamera_t2668956642_StaticFields*)PixelPerfectCamera_t2668956642_il2cpp_TypeInfo_var->static_fields)->get_pixelsToUnits_2();
		float L_7 = ((PixelPerfectCamera_t2668956642_StaticFields*)PixelPerfectCamera_t2668956642_il2cpp_TypeInfo_var->static_fields)->get_scale_3();
		((PixelPerfectCamera_t2668956642_StaticFields*)PixelPerfectCamera_t2668956642_il2cpp_TypeInfo_var->static_fields)->set_pixelsToUnits_2(((float)((float)L_6*(float)L_7)));
		Camera_t2805735124 * L_8 = V_0;
		int32_t L_9 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_10 = ((PixelPerfectCamera_t2668956642_StaticFields*)PixelPerfectCamera_t2668956642_il2cpp_TypeInfo_var->static_fields)->get_pixelsToUnits_2();
		NullCheck(L_8);
		Camera_set_orthographicSize_m2708824189(L_8, ((float)((float)((float)((float)(((float)((float)L_9)))/(float)(2.0f)))/(float)L_10)), /*hidden argument*/NULL);
	}

IL_0051:
	{
		return;
	}
}
// System.Void PrepareToDuel::.ctor()
extern TypeInfo* Vector2_t465617798_il2cpp_TypeInfo_var;
extern const uint32_t PrepareToDuel__ctor_m939879657_MetadataUsageId;
extern "C"  void PrepareToDuel__ctor_m939879657 (PrepareToDuel_t908926924 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrepareToDuel__ctor_m939879657_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t465617798  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Vector2_t465617798_il2cpp_TypeInfo_var, (&V_0));
		Vector2_t465617798  L_0 = V_0;
		__this->set_sideTo_4(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PrepareToDuel::.cctor()
extern "C"  void PrepareToDuel__cctor_m2023699102 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean PrepareToDuel::get_playerStoped()
extern TypeInfo* PrepareToDuel_t908926924_il2cpp_TypeInfo_var;
extern const uint32_t PrepareToDuel_get_playerStoped_m3146878400_MetadataUsageId;
extern "C"  bool PrepareToDuel_get_playerStoped_m3146878400 (PrepareToDuel_t908926924 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrepareToDuel_get_playerStoped_m3146878400_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PrepareToDuel_t908926924_il2cpp_TypeInfo_var);
		bool L_0 = ((PrepareToDuel_t908926924_StaticFields*)PrepareToDuel_t908926924_il2cpp_TypeInfo_var->static_fields)->get_playerStopedController_8();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return (bool)0;
	}

IL_000c:
	{
		bool L_1 = __this->get__playerStoped_9();
		return L_1;
	}
}
// System.Void PrepareToDuel::set_playerStoped(System.Boolean)
extern "C"  void PrepareToDuel_set_playerStoped_m2692168537 (PrepareToDuel_t908926924 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->set__playerStoped_9(L_0);
		return;
	}
}
// System.Void PrepareToDuel::Start()
extern "C"  void PrepareToDuel_Start_m712267537 (PrepareToDuel_t908926924 * __this, const MethodInfo* method)
{
	{
		PrepareToDuel_CalculateStopPoints_m2830488464(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PrepareToDuel::Update()
extern TypeInfo* GameStateManager_t1063875770_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t1692945841_il2cpp_TypeInfo_var;
extern TypeInfo* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern TypeInfo* PrepareToDuel_t908926924_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimationManager_t2328214095_m400137410_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral868748054;
extern const uint32_t PrepareToDuel_Update_m4209704278_MetadataUsageId;
extern "C"  void PrepareToDuel_Update_m4209704278 (PrepareToDuel_t908926924 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PrepareToDuel_Update_m4209704278_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t465617797  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_0 = GameStateManager_get_currentGameState_m2409036446(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_00f9;
		}
	}
	{
		AnimationManager_t2328214095 * L_1 = __this->get_animator_6();
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, (Object_t1181371020 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		AnimationManager_t2328214095 * L_3 = Component_GetComponent_TisAnimationManager_t2328214095_m400137410(__this, /*hidden argument*/Component_GetComponent_TisAnimationManager_t2328214095_m400137410_MethodInfo_var);
		__this->set_animator_6(L_3);
	}

IL_0028:
	{
		Transform_t224878301 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t465617797  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		float L_6 = (&V_0)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1692945841_il2cpp_TypeInfo_var);
		float L_7 = fabsf(L_6);
		float L_8 = __this->get_stopPointX_5();
		float L_9 = fabsf(L_8);
		if ((!(((float)L_7) > ((float)L_9))))
		{
			goto IL_00a6;
		}
	}
	{
		Transform_t224878301 * L_10 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector2_t465617798  L_11 = __this->get_sideTo_4();
		float L_12 = __this->get_speed_3();
		Vector2_t465617798  L_13 = Vector2_op_Multiply_m4236139442(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		float L_14 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t465617798  L_15 = Vector2_op_Multiply_m4236139442(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Vector3_t465617797  L_16 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_Translate_m3316827744(L_10, L_16, /*hidden argument*/NULL);
		bool L_17 = __this->get_cowboys_move_to_center_started_7();
		if (L_17)
		{
			goto IL_00a1;
		}
	}
	{
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, _stringLiteral868748054, /*hidden argument*/NULL);
		__this->set_cowboys_move_to_center_started_7((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(SoundManager_t654432262_il2cpp_TypeInfo_var);
		SoundManager_t654432262 * L_18 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_3();
		NullCheck(L_18);
		SoundManager_playCowboysMoveToCenterMusic_m3336171715(L_18, /*hidden argument*/NULL);
	}

IL_00a1:
	{
		goto IL_00f9;
	}

IL_00a6:
	{
		__this->set_cowboys_move_to_center_started_7((bool)0);
		bool L_19 = PrepareToDuel_get_playerStoped_m3146878400(__this, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00dc;
		}
	}
	{
		PrepareToDuel_set_playerStoped_m2692168537(__this, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PrepareToDuel_t908926924_il2cpp_TypeInfo_var);
		((PrepareToDuel_t908926924_StaticFields*)PrepareToDuel_t908926924_il2cpp_TypeInfo_var->static_fields)->set_playerStopedController_8((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_20 = ((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get_cowboysReadyForDuel_4();
		((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->set_cowboysReadyForDuel_4(((int32_t)((int32_t)L_20+(int32_t)1)));
		AnimationManager_t2328214095 * L_21 = __this->get_animator_6();
		NullCheck(L_21);
		AnimationManager_Idle_m2360568088(L_21, /*hidden argument*/NULL);
	}

IL_00dc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_22 = ((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get_cowboysReadyForDuel_4();
		if ((!(((uint32_t)L_22) == ((uint32_t)2))))
		{
			goto IL_00f9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		GameStateManager_set_currentGameState_m1712983881(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->set_cowboysReadyForDuel_4(0);
		IL2CPP_RUNTIME_CLASS_INIT(PrepareToDuel_t908926924_il2cpp_TypeInfo_var);
		((PrepareToDuel_t908926924_StaticFields*)PrepareToDuel_t908926924_il2cpp_TypeInfo_var->static_fields)->set_playerStopedController_8((bool)0);
	}

IL_00f9:
	{
		return;
	}
}
// System.Void PrepareToDuel::CalculateStopPoints()
extern "C"  void PrepareToDuel_CalculateStopPoints_m2830488464 (PrepareToDuel_t908926924 * __this, const MethodInfo* method)
{
	Vector3_t465617797  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t224878301 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t465617797  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_x_1();
		if ((!(((float)L_2) < ((float)(0.0f)))))
		{
			goto IL_003a;
		}
	}
	{
		float L_3 = __this->get_pixelsFromCenter_2();
		__this->set_stopPointX_5(((-L_3)));
		Vector2_t465617798  L_4 = Vector2_get_right_m28012078(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_sideTo_4(L_4);
		goto IL_0051;
	}

IL_003a:
	{
		float L_5 = __this->get_pixelsFromCenter_2();
		__this->set_stopPointX_5(L_5);
		Vector2_t465617798  L_6 = Vector2_get_left_m573266379(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_sideTo_4(L_6);
	}

IL_0051:
	{
		return;
	}
}
// System.Void ReSkinAnimation::.ctor()
extern "C"  void ReSkinAnimation__ctor_m1158818905 (ReSkinAnimation_t3053752670 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ReSkinAnimation::Awake()
extern const MethodInfo* Component_GetComponent_TisBoxCollider2D_t2914288752_m324820273_MethodInfo_var;
extern const uint32_t ReSkinAnimation_Awake_m3510018730_MetadataUsageId;
extern "C"  void ReSkinAnimation_Awake_m3510018730 (ReSkinAnimation_t3053752670 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReSkinAnimation_Awake_m3510018730_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BoxCollider2D_t2914288752 * L_0 = Component_GetComponent_TisBoxCollider2D_t2914288752_m324820273(__this, /*hidden argument*/Component_GetComponent_TisBoxCollider2D_t2914288752_m324820273_MethodInfo_var);
		__this->set_dynamicCollider_3(L_0);
		return;
	}
}
// System.Void ReSkinAnimation::LateUpdate()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* U3CLateUpdateU3Ec__AnonStorey6_t4292923148_il2cpp_TypeInfo_var;
extern TypeInfo* Predicate_1_t231192664_il2cpp_TypeInfo_var;
extern const MethodInfo* Resources_LoadAll_TisSprite_t1118776648_m2824615481_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisSpriteRenderer_t3863917503_m1456864797_MethodInfo_var;
extern const MethodInfo* U3CLateUpdateU3Ec__AnonStorey6_U3CU3Em__0_m4006040457_MethodInfo_var;
extern const MethodInfo* Predicate_1__ctor_m4078880280_MethodInfo_var;
extern const MethodInfo* Array_Find_TisSprite_t1118776648_m3404915674_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral922452174;
extern const uint32_t ReSkinAnimation_LateUpdate_m269496232_MetadataUsageId;
extern "C"  void ReSkinAnimation_LateUpdate_m269496232 (ReSkinAnimation_t3053752670 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReSkinAnimation_LateUpdate_m269496232_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SpriteU5BU5D_t4275260569* V_0 = NULL;
	SpriteRenderer_t3863917503 * V_1 = NULL;
	SpriteRendererU5BU5D_t1066967654* V_2 = NULL;
	int32_t V_3 = 0;
	Sprite_t1118776648 * V_4 = NULL;
	U3CLateUpdateU3Ec__AnonStorey6_t4292923148 * V_5 = NULL;
	Bounds_t968365060  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		String_t* L_0 = __this->get_spriteSheetName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral922452174, L_0, /*hidden argument*/NULL);
		SpriteU5BU5D_t4275260569* L_2 = Resources_LoadAll_TisSprite_t1118776648_m2824615481(NULL /*static, unused*/, L_1, /*hidden argument*/Resources_LoadAll_TisSprite_t1118776648_m2824615481_MethodInfo_var);
		V_0 = L_2;
		SpriteRendererU5BU5D_t1066967654* L_3 = Component_GetComponentsInChildren_TisSpriteRenderer_t3863917503_m1456864797(__this, /*hidden argument*/Component_GetComponentsInChildren_TisSpriteRenderer_t3863917503_m1456864797_MethodInfo_var);
		V_2 = L_3;
		V_3 = 0;
		goto IL_008e;
	}

IL_0024:
	{
		SpriteRendererU5BU5D_t1066967654* L_4 = V_2;
		int32_t L_5 = V_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_1 = ((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
		U3CLateUpdateU3Ec__AnonStorey6_t4292923148 * L_7 = (U3CLateUpdateU3Ec__AnonStorey6_t4292923148 *)il2cpp_codegen_object_new(U3CLateUpdateU3Ec__AnonStorey6_t4292923148_il2cpp_TypeInfo_var);
		U3CLateUpdateU3Ec__AnonStorey6__ctor_m2047503238(L_7, /*hidden argument*/NULL);
		V_5 = L_7;
		U3CLateUpdateU3Ec__AnonStorey6_t4292923148 * L_8 = V_5;
		SpriteRenderer_t3863917503 * L_9 = V_1;
		NullCheck(L_9);
		Sprite_t1118776648 * L_10 = SpriteRenderer_get_sprite_m3016837432(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11 = Object_get_name_m2079638459(L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		L_8->set_spriteName_0(L_11);
		SpriteU5BU5D_t4275260569* L_12 = V_0;
		U3CLateUpdateU3Ec__AnonStorey6_t4292923148 * L_13 = V_5;
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)U3CLateUpdateU3Ec__AnonStorey6_U3CU3Em__0_m4006040457_MethodInfo_var);
		Predicate_1_t231192664 * L_15 = (Predicate_1_t231192664 *)il2cpp_codegen_object_new(Predicate_1_t231192664_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m4078880280(L_15, L_13, L_14, /*hidden argument*/Predicate_1__ctor_m4078880280_MethodInfo_var);
		Sprite_t1118776648 * L_16 = Array_Find_TisSprite_t1118776648_m3404915674(NULL /*static, unused*/, L_12, L_15, /*hidden argument*/Array_Find_TisSprite_t1118776648_m3404915674_MethodInfo_var);
		V_4 = L_16;
		Sprite_t1118776648 * L_17 = V_4;
		bool L_18 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_008a;
		}
	}
	{
		SpriteRenderer_t3863917503 * L_19 = V_1;
		Sprite_t1118776648 * L_20 = V_4;
		NullCheck(L_19);
		SpriteRenderer_set_sprite_m617298623(L_19, L_20, /*hidden argument*/NULL);
		BoxCollider2D_t2914288752 * L_21 = __this->get_dynamicCollider_3();
		Sprite_t1118776648 * L_22 = V_4;
		NullCheck(L_22);
		Bounds_t968365060  L_23 = Sprite_get_bounds_m31809269(L_22, /*hidden argument*/NULL);
		V_6 = L_23;
		Vector3_t465617797  L_24 = Bounds_get_size_m1728027642((&V_6), /*hidden argument*/NULL);
		Vector2_t465617798  L_25 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		NullCheck(L_21);
		BoxCollider2D_set_size_m3505152706(L_21, L_25, /*hidden argument*/NULL);
	}

IL_008a:
	{
		int32_t L_26 = V_3;
		V_3 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_008e:
	{
		int32_t L_27 = V_3;
		SpriteRendererU5BU5D_t1066967654* L_28 = V_2;
		NullCheck(L_28);
		if ((((int32_t)L_27) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		return;
	}
}
// System.Void ReSkinAnimation/<LateUpdate>c__AnonStorey6::.ctor()
extern "C"  void U3CLateUpdateU3Ec__AnonStorey6__ctor_m2047503238 (U3CLateUpdateU3Ec__AnonStorey6_t4292923148 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ReSkinAnimation/<LateUpdate>c__AnonStorey6::<>m__0(UnityEngine.Sprite)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const uint32_t U3CLateUpdateU3Ec__AnonStorey6_U3CU3Em__0_m4006040457_MetadataUsageId;
extern "C"  bool U3CLateUpdateU3Ec__AnonStorey6_U3CU3Em__0_m4006040457 (U3CLateUpdateU3Ec__AnonStorey6_t4292923148 * __this, Sprite_t1118776648 * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLateUpdateU3Ec__AnonStorey6_U3CU3Em__0_m4006040457_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Sprite_t1118776648 * L_0 = ___item;
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m2079638459(L_0, /*hidden argument*/NULL);
		String_t* L_2 = __this->get_spriteName_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void Rotator::.ctor()
extern "C"  void Rotator__ctor_m1896178780 (Rotator_t3375904803 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Rotator::Start()
extern "C"  void Rotator_Start_m1112542632 (Rotator_t3375904803 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_angle_2();
		Vector3_t465617797  L_1 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t83606849  L_2 = Quaternion_AngleAxis_m2806222563(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_qStart_4(L_2);
		float L_3 = __this->get_angle_2();
		Vector3_t465617797  L_4 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t83606849  L_5 = Quaternion_AngleAxis_m2806222563(NULL /*static, unused*/, ((-L_3)), L_4, /*hidden argument*/NULL);
		__this->set_qEnd_5(L_5);
		return;
	}
}
// System.Void Rotator::Update()
extern TypeInfo* Mathf_t1692945841_il2cpp_TypeInfo_var;
extern const uint32_t Rotator_Update_m12837135_MetadataUsageId;
extern "C"  void Rotator_Update_m12837135 (Rotator_t3375904803 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Rotator_Update_m12837135_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t224878301 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t83606849  L_1 = __this->get_qStart_4();
		Quaternion_t83606849  L_2 = __this->get_qEnd_5();
		float L_3 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = __this->get_speed_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1692945841_il2cpp_TypeInfo_var);
		float L_5 = sinf(((float)((float)L_3*(float)L_4)));
		Quaternion_t83606849  L_6 = Quaternion_Lerp_m3623055223(NULL /*static, unused*/, L_1, L_2, ((float)((float)((float)((float)L_5+(float)(1.0f)))/(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_rotation_m3411284563(L_0, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Shot::.ctor()
extern "C"  void Shot__ctor_m1262883755 (Shot_t3441902592 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Shot::.cctor()
extern "C"  void Shot__cctor_m408840314 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Shot::Start()
extern const MethodInfo* Component_GetComponent_TisAnimationManager_t2328214095_m400137410_MethodInfo_var;
extern const uint32_t Shot_Start_m850548415_MetadataUsageId;
extern "C"  void Shot_Start_m850548415 (Shot_t3441902592 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Shot_Start_m850548415_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AnimationManager_t2328214095 * L_0 = Component_GetComponent_TisAnimationManager_t2328214095_m400137410(__this, /*hidden argument*/Component_GetComponent_TisAnimationManager_t2328214095_m400137410_MethodInfo_var);
		__this->set_animator_2(L_0);
		return;
	}
}
// System.Void Shot::Update()
extern TypeInfo* GameStateManager_t1063875770_il2cpp_TypeInfo_var;
extern TypeInfo* Shot_t3441902592_il2cpp_TypeInfo_var;
extern TypeInfo* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t4173266137_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t3863917503_m1184556631_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3820598586;
extern Il2CppCodeGenString* _stringLiteral1973084735;
extern Il2CppCodeGenString* _stringLiteral1165420408;
extern Il2CppCodeGenString* _stringLiteral2345132694;
extern const uint32_t Shot_Update_m3469805538_MetadataUsageId;
extern "C"  void Shot_Update_m3469805538 (Shot_t3441902592 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Shot_Update_m3469805538_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SpriteRenderer_t3863917503 * V_0 = NULL;
	Vector3_t465617797  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t465617797  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t465617797  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t465617797  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_0 = GameStateManager_get_currentGameState_m2409036446(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)4))))
		{
			goto IL_01f6;
		}
	}
	{
		GameObject_t1366199518 * L_1 = __this->get_playerLeft_3();
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, (Object_t1181371020 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		GameObject_t1366199518 * L_3 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral3820598586, /*hidden argument*/NULL);
		__this->set_playerLeft_3(L_3);
	}

IL_002c:
	{
		GameObject_t1366199518 * L_4 = __this->get_playerRight_4();
		bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_4, (Object_t1181371020 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004d;
		}
	}
	{
		GameObject_t1366199518 * L_6 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral1973084735, /*hidden argument*/NULL);
		__this->set_playerRight_4(L_6);
	}

IL_004d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Shot_t3441902592_il2cpp_TypeInfo_var);
		bool L_7 = ((Shot_t3441902592_StaticFields*)Shot_t3441902592_il2cpp_TypeInfo_var->static_fields)->get_goToShotSoundPlayed_6();
		if (L_7)
		{
			goto IL_0067;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManager_t654432262_il2cpp_TypeInfo_var);
		SoundManager_t654432262 * L_8 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_3();
		NullCheck(L_8);
		SoundManager_PlayFire_m1786007431(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Shot_t3441902592_il2cpp_TypeInfo_var);
		((Shot_t3441902592_StaticFields*)Shot_t3441902592_il2cpp_TypeInfo_var->static_fields)->set_goToShotSoundPlayed_6((bool)1);
	}

IL_0067:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4173266137_il2cpp_TypeInfo_var);
		bool L_9 = Input_GetMouseButtonDown_m47917805(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_01f6;
		}
	}
	{
		Transform_t224878301 * L_10 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t465617797  L_11 = Transform_get_position_m1104419803(L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		float L_12 = (&V_1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4173266137_il2cpp_TypeInfo_var);
		Vector3_t465617797  L_13 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_13;
		float L_14 = (&V_2)->get_x_1();
		int32_t L_15 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!((int32_t)((int32_t)((((float)L_12) < ((float)(0.0f)))? 1 : 0)&(int32_t)((((float)L_14) < ((float)(((float)((float)((int32_t)((int32_t)L_15/(int32_t)2)))))))? 1 : 0))))
		{
			goto IL_013d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Shot_t3441902592_il2cpp_TypeInfo_var);
		bool L_16 = ((Shot_t3441902592_StaticFields*)Shot_t3441902592_il2cpp_TypeInfo_var->static_fields)->get_alreadyShot_5();
		if (L_16)
		{
			goto IL_013d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Shot_t3441902592_il2cpp_TypeInfo_var);
		((Shot_t3441902592_StaticFields*)Shot_t3441902592_il2cpp_TypeInfo_var->static_fields)->set_alreadyShot_5((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_17 = ((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get_scoreLeft_5();
		((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->set_scoreLeft_5(((int32_t)((int32_t)L_17+(int32_t)1)));
		AnimationManager_t2328214095 * L_18 = __this->get_animator_2();
		NullCheck(L_18);
		AnimationManager_Shot_m2722230414(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SoundManager_t654432262_il2cpp_TypeInfo_var);
		SoundManager_t654432262 * L_19 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_3();
		NullCheck(L_19);
		SoundManager_PlaySuicideSound_m707867868(L_19, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_20 = __this->get_playerRight_4();
		NullCheck(L_20);
		SpriteRenderer_t3863917503 * L_21 = GameObject_GetComponent_TisSpriteRenderer_t3863917503_m1184556631(L_20, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3863917503_m1184556631_MethodInfo_var);
		V_0 = L_21;
		SpriteRenderer_t3863917503 * L_22 = V_0;
		NullCheck(L_22);
		SpriteRenderer_set_flipX_m994174828(L_22, (bool)0, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_23 = __this->get_playerRight_4();
		NullCheck(L_23);
		GameObject_SendMessage_m1177535567(L_23, _stringLiteral1165420408, /*hidden argument*/NULL);
		((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->set_needToSpawn_7(1);
		int32_t L_24 = ((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get_scoreLeft_5();
		int32_t L_25 = ((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get_maxScore_2();
		if ((((int32_t)L_24) == ((int32_t)L_25)))
		{
			goto IL_0121;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_26 = ((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get_scoreRight_6();
		int32_t L_27 = ((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get_maxScore_2();
		if ((!(((uint32_t)L_26) == ((uint32_t)L_27))))
		{
			goto IL_012c;
		}
	}

IL_0121:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		GameStateManager_set_currentGameState_m1712983881(NULL /*static, unused*/, 6, /*hidden argument*/NULL);
		goto IL_013d;
	}

IL_012c:
	{
		MonoBehaviour_StartCoroutine_m1399371129(__this, _stringLiteral2345132694, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		GameStateManager_ResetRandomDelay_m2974787186(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_013d:
	{
		Transform_t224878301 * L_28 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_28);
		Vector3_t465617797  L_29 = Transform_get_position_m1104419803(L_28, /*hidden argument*/NULL);
		V_3 = L_29;
		float L_30 = (&V_3)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4173266137_il2cpp_TypeInfo_var);
		Vector3_t465617797  L_31 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_31;
		float L_32 = (&V_4)->get_x_1();
		int32_t L_33 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!((int32_t)((int32_t)((((float)L_30) > ((float)(0.0f)))? 1 : 0)&(int32_t)((((float)L_32) > ((float)(((float)((float)((int32_t)((int32_t)L_33/(int32_t)2)))))))? 1 : 0))))
		{
			goto IL_01f6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Shot_t3441902592_il2cpp_TypeInfo_var);
		bool L_34 = ((Shot_t3441902592_StaticFields*)Shot_t3441902592_il2cpp_TypeInfo_var->static_fields)->get_alreadyShot_5();
		if (L_34)
		{
			goto IL_01f6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Shot_t3441902592_il2cpp_TypeInfo_var);
		((Shot_t3441902592_StaticFields*)Shot_t3441902592_il2cpp_TypeInfo_var->static_fields)->set_alreadyShot_5((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_35 = ((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get_scoreRight_6();
		((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->set_scoreRight_6(((int32_t)((int32_t)L_35+(int32_t)1)));
		AnimationManager_t2328214095 * L_36 = __this->get_animator_2();
		NullCheck(L_36);
		AnimationManager_Shot_m2722230414(L_36, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SoundManager_t654432262_il2cpp_TypeInfo_var);
		SoundManager_t654432262 * L_37 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_3();
		NullCheck(L_37);
		SoundManager_PlaySuicideSound_m707867868(L_37, /*hidden argument*/NULL);
		GameObject_t1366199518 * L_38 = __this->get_playerLeft_3();
		NullCheck(L_38);
		GameObject_SendMessage_m1177535567(L_38, _stringLiteral1165420408, /*hidden argument*/NULL);
		((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->set_needToSpawn_7(0);
		int32_t L_39 = ((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get_scoreRight_6();
		int32_t L_40 = ((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get_maxScore_2();
		if ((((int32_t)L_39) == ((int32_t)L_40)))
		{
			goto IL_01da;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_41 = ((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get_scoreLeft_5();
		int32_t L_42 = ((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get_maxScore_2();
		if ((!(((uint32_t)L_41) == ((uint32_t)L_42))))
		{
			goto IL_01e5;
		}
	}

IL_01da:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		GameStateManager_set_currentGameState_m1712983881(NULL /*static, unused*/, 6, /*hidden argument*/NULL);
		goto IL_01f6;
	}

IL_01e5:
	{
		MonoBehaviour_StartCoroutine_m1399371129(__this, _stringLiteral2345132694, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		GameStateManager_ResetRandomDelay_m2974787186(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_01f6:
	{
		return;
	}
}
// System.Collections.IEnumerator Shot::WaitForSpawnNewCowboy()
extern TypeInfo* U3CWaitForSpawnNewCowboyU3Ec__Iterator3_t319929336_il2cpp_TypeInfo_var;
extern const uint32_t Shot_WaitForSpawnNewCowboy_m432840889_MetadataUsageId;
extern "C"  Il2CppObject * Shot_WaitForSpawnNewCowboy_m432840889 (Shot_t3441902592 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Shot_WaitForSpawnNewCowboy_m432840889_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CWaitForSpawnNewCowboyU3Ec__Iterator3_t319929336 * V_0 = NULL;
	{
		U3CWaitForSpawnNewCowboyU3Ec__Iterator3_t319929336 * L_0 = (U3CWaitForSpawnNewCowboyU3Ec__Iterator3_t319929336 *)il2cpp_codegen_object_new(U3CWaitForSpawnNewCowboyU3Ec__Iterator3_t319929336_il2cpp_TypeInfo_var);
		U3CWaitForSpawnNewCowboyU3Ec__Iterator3__ctor_m1955491998(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitForSpawnNewCowboyU3Ec__Iterator3_t319929336 * L_1 = V_0;
		return L_1;
	}
}
// System.Void Shot/<WaitForSpawnNewCowboy>c__Iterator3::.ctor()
extern "C"  void U3CWaitForSpawnNewCowboyU3Ec__Iterator3__ctor_m1955491998 (U3CWaitForSpawnNewCowboyU3Ec__Iterator3_t319929336 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Shot/<WaitForSpawnNewCowboy>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForSpawnNewCowboyU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1731888494 (U3CWaitForSpawnNewCowboyU3Ec__Iterator3_t319929336 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Shot/<WaitForSpawnNewCowboy>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForSpawnNewCowboyU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1977705126 (U3CWaitForSpawnNewCowboyU3Ec__Iterator3_t319929336 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean Shot/<WaitForSpawnNewCowboy>c__Iterator3::MoveNext()
extern TypeInfo* WaitForSeconds_t1717981302_il2cpp_TypeInfo_var;
extern TypeInfo* Shot_t3441902592_il2cpp_TypeInfo_var;
extern TypeInfo* GameStateManager_t1063875770_il2cpp_TypeInfo_var;
extern const uint32_t U3CWaitForSpawnNewCowboyU3Ec__Iterator3_MoveNext_m1487698266_MetadataUsageId;
extern "C"  bool U3CWaitForSpawnNewCowboyU3Ec__Iterator3_MoveNext_m1487698266 (U3CWaitForSpawnNewCowboyU3Ec__Iterator3_t319929336 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitForSpawnNewCowboyU3Ec__Iterator3_MoveNext_m1487698266_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003d;
		}
	}
	{
		goto IL_0050;
	}

IL_0021:
	{
		WaitForSeconds_t1717981302 * L_2 = (WaitForSeconds_t1717981302 *)il2cpp_codegen_object_new(WaitForSeconds_t1717981302_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_2, (3.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		__this->set_U24PC_0(1);
		goto IL_0052;
	}

IL_003d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Shot_t3441902592_il2cpp_TypeInfo_var);
		((Shot_t3441902592_StaticFields*)Shot_t3441902592_il2cpp_TypeInfo_var->static_fields)->set_goToShotSoundPlayed_6((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		GameStateManager_set_currentGameState_m1712983881(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		__this->set_U24PC_0((-1));
	}

IL_0050:
	{
		return (bool)0;
	}

IL_0052:
	{
		return (bool)1;
	}
	// Dead block : IL_0054: ldloc.1
}
// System.Void Shot/<WaitForSpawnNewCowboy>c__Iterator3::Dispose()
extern "C"  void U3CWaitForSpawnNewCowboyU3Ec__Iterator3_Dispose_m2695425855 (U3CWaitForSpawnNewCowboyU3Ec__Iterator3_t319929336 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void Shot/<WaitForSpawnNewCowboy>c__Iterator3::Reset()
extern TypeInfo* NotSupportedException_t3178859535_il2cpp_TypeInfo_var;
extern const uint32_t U3CWaitForSpawnNewCowboyU3Ec__Iterator3_Reset_m3401338405_MetadataUsageId;
extern "C"  void U3CWaitForSpawnNewCowboyU3Ec__Iterator3_Reset_m3401338405 (U3CWaitForSpawnNewCowboyU3Ec__Iterator3_t319929336 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitForSpawnNewCowboyU3Ec__Iterator3_Reset_m3401338405_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t3178859535 * L_0 = (NotSupportedException_t3178859535 *)il2cpp_codegen_object_new(NotSupportedException_t3178859535_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ShowScore::.ctor()
extern "C"  void ShowScore__ctor_m4205901544 (ShowScore_t4012426113 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShowScore::Start()
extern "C"  void ShowScore_Start_m298709576 (ShowScore_t4012426113 * __this, const MethodInfo* method)
{
	{
		Text_t3921196294 * L_0 = __this->get_leftScore_2();
		NullCheck(L_0);
		GameObject_t1366199518 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
		Text_t3921196294 * L_2 = __this->get_rightScore_3();
		NullCheck(L_2);
		GameObject_t1366199518 * L_3 = Component_get_gameObject_m3105766835(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShowScore::Update()
extern TypeInfo* GameStateManager_t1063875770_il2cpp_TypeInfo_var;
extern const uint32_t ShowScore_Update_m2046544741_MetadataUsageId;
extern "C"  void ShowScore_Update_m2046544741 (ShowScore_t4012426113 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShowScore_Update_m2046544741_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_0 = GameStateManager_get_currentGameState_m2409036446(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0056;
		}
	}
	{
		Text_t3921196294 * L_1 = __this->get_leftScore_2();
		NullCheck(L_1);
		GameObject_t1366199518 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)1, /*hidden argument*/NULL);
		Text_t3921196294 * L_3 = __this->get_rightScore_3();
		NullCheck(L_3);
		GameObject_t1366199518 * L_4 = Component_get_gameObject_m3105766835(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, (bool)1, /*hidden argument*/NULL);
		Text_t3921196294 * L_5 = __this->get_leftScore_2();
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		String_t* L_6 = Int32_ToString_m2960866144((((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get_address_of_scoreLeft_5()), /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_6);
		Text_t3921196294 * L_7 = __this->get_rightScore_3();
		String_t* L_8 = Int32_ToString_m2960866144((((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get_address_of_scoreRight_6()), /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_7, L_8);
	}

IL_0056:
	{
		return;
	}
}
// System.Void SoundManager::.ctor()
extern "C"  void SoundManager__ctor_m3417712111 (SoundManager_t654432262 * __this, const MethodInfo* method)
{
	{
		__this->set_lowPitchRange_4((0.95f));
		__this->set_highPitchRange_5((1.05f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundManager::.cctor()
extern "C"  void SoundManager__cctor_m1047370624 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SoundManager::Awake()
extern TypeInfo* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern const uint32_t SoundManager_Awake_m1006343474_MetadataUsageId;
extern "C"  void SoundManager_Awake_m1006343474 (SoundManager_t654432262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SoundManager_Awake_m1006343474_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManager_t654432262_il2cpp_TypeInfo_var);
		SoundManager_t654432262 * L_0 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_3();
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1181371020 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManager_t654432262_il2cpp_TypeInfo_var);
		((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->set_instance_3(__this);
		goto IL_0036;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManager_t654432262_il2cpp_TypeInfo_var);
		SoundManager_t654432262 * L_2 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_3();
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, __this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		GameObject_t1366199518 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0036:
	{
		GameObject_t1366199518 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundManager::PlaySingle(UnityEngine.AudioClip)
extern "C"  void SoundManager_PlaySingle_m2017509048 (SoundManager_t654432262 * __this, AudioClip_t3927647597 * ___clip, const MethodInfo* method)
{
	{
		AudioSource_t585923902 * L_0 = __this->get_efxSource_2();
		AudioClip_t3927647597 * L_1 = ___clip;
		NullCheck(L_0);
		AudioSource_set_clip_m738814682(L_0, L_1, /*hidden argument*/NULL);
		AudioSource_t585923902 * L_2 = __this->get_efxSource_2();
		NullCheck(L_2);
		AudioSource_Play_m353744792(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundManager::RandomizeSfx(UnityEngine.AudioClip[])
extern "C"  void SoundManager_RandomizeSfx_m3482976376 (SoundManager_t654432262 * __this, AudioClipU5BU5D_t3948543552* ___clips, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		AudioClipU5BU5D_t3948543552* L_0 = ___clips;
		NullCheck(L_0);
		int32_t L_1 = Random_Range_m694320887(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length)))), /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = __this->get_lowPitchRange_4();
		float L_3 = __this->get_highPitchRange_5();
		float L_4 = Random_Range_m2884721203(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		AudioSource_t585923902 * L_5 = __this->get_efxSource_2();
		float L_6 = V_1;
		NullCheck(L_5);
		AudioSource_set_pitch_m3064416458(L_5, L_6, /*hidden argument*/NULL);
		AudioSource_t585923902 * L_7 = __this->get_efxSource_2();
		AudioClipU5BU5D_t3948543552* L_8 = ___clips;
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		NullCheck(L_7);
		AudioSource_set_clip_m738814682(L_7, ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10))), /*hidden argument*/NULL);
		AudioSource_t585923902 * L_11 = __this->get_efxSource_2();
		NullCheck(L_11);
		AudioSource_Play_m353744792(L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundManager::playCowboysMoveToCenterMusic()
extern TypeInfo* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern TypeInfo* AudioClipU5BU5D_t3948543552_il2cpp_TypeInfo_var;
extern const uint32_t SoundManager_playCowboysMoveToCenterMusic_m3336171715_MetadataUsageId;
extern "C"  void SoundManager_playCowboysMoveToCenterMusic_m3336171715 (SoundManager_t654432262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SoundManager_playCowboysMoveToCenterMusic_m3336171715_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManager_t654432262_il2cpp_TypeInfo_var);
		SoundManager_t654432262 * L_0 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_3();
		AudioClipU5BU5D_t3948543552* L_1 = ((AudioClipU5BU5D_t3948543552*)SZArrayNew(AudioClipU5BU5D_t3948543552_il2cpp_TypeInfo_var, (uint32_t)1));
		AudioClip_t3927647597 * L_2 = __this->get_cowboys_move_to_center_6();
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (AudioClip_t3927647597 *)L_2);
		NullCheck(L_0);
		SoundManager_RandomizeSfx_m3482976376(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundManager::PlaySuicideSound()
extern TypeInfo* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern TypeInfo* AudioClipU5BU5D_t3948543552_il2cpp_TypeInfo_var;
extern const uint32_t SoundManager_PlaySuicideSound_m707867868_MetadataUsageId;
extern "C"  void SoundManager_PlaySuicideSound_m707867868 (SoundManager_t654432262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SoundManager_PlaySuicideSound_m707867868_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManager_t654432262_il2cpp_TypeInfo_var);
		SoundManager_t654432262 * L_0 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_3();
		AudioClipU5BU5D_t3948543552* L_1 = ((AudioClipU5BU5D_t3948543552*)SZArrayNew(AudioClipU5BU5D_t3948543552_il2cpp_TypeInfo_var, (uint32_t)1));
		AudioClip_t3927647597 * L_2 = __this->get_suicideSound_7();
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (AudioClip_t3927647597 *)L_2);
		NullCheck(L_0);
		SoundManager_RandomizeSfx_m3482976376(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundManager::PlayFire()
extern TypeInfo* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern TypeInfo* AudioClipU5BU5D_t3948543552_il2cpp_TypeInfo_var;
extern const uint32_t SoundManager_PlayFire_m1786007431_MetadataUsageId;
extern "C"  void SoundManager_PlayFire_m1786007431 (SoundManager_t654432262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SoundManager_PlayFire_m1786007431_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManager_t654432262_il2cpp_TypeInfo_var);
		SoundManager_t654432262 * L_0 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_3();
		AudioClipU5BU5D_t3948543552* L_1 = ((AudioClipU5BU5D_t3948543552*)SZArrayNew(AudioClipU5BU5D_t3948543552_il2cpp_TypeInfo_var, (uint32_t)1));
		AudioClip_t3927647597 * L_2 = __this->get_fireSound_8();
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (AudioClip_t3927647597 *)L_2);
		NullCheck(L_0);
		SoundManager_RandomizeSfx_m3482976376(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundManager::PlayWinStateMusic()
extern TypeInfo* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern TypeInfo* AudioClipU5BU5D_t3948543552_il2cpp_TypeInfo_var;
extern const uint32_t SoundManager_PlayWinStateMusic_m2740338453_MetadataUsageId;
extern "C"  void SoundManager_PlayWinStateMusic_m2740338453 (SoundManager_t654432262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SoundManager_PlayWinStateMusic_m2740338453_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManager_t654432262_il2cpp_TypeInfo_var);
		SoundManager_t654432262 * L_0 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_3();
		AudioClipU5BU5D_t3948543552* L_1 = ((AudioClipU5BU5D_t3948543552*)SZArrayNew(AudioClipU5BU5D_t3948543552_il2cpp_TypeInfo_var, (uint32_t)1));
		AudioClip_t3927647597 * L_2 = __this->get_winMusic_9();
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (AudioClip_t3927647597 *)L_2);
		NullCheck(L_0);
		SoundManager_RandomizeSfx_m3482976376(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Spawner::.ctor()
extern "C"  void Spawner__ctor_m1860016179 (Spawner_t534830648 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Spawner::.cctor()
extern "C"  void Spawner__cctor_m869600462 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Spawner::Update()
extern TypeInfo* GameStateManager_t1063875770_il2cpp_TypeInfo_var;
extern TypeInfo* Shot_t3441902592_il2cpp_TypeInfo_var;
extern const uint32_t Spawner_Update_m1485478966_MetadataUsageId;
extern "C"  void Spawner_Update_m1485478966 (Spawner_t534830648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Spawner_Update_m1485478966_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_0 = GameStateManager_get_currentGameState_m2409036446(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0066;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Shot_t3441902592_il2cpp_TypeInfo_var);
		((Shot_t3441902592_StaticFields*)Shot_t3441902592_il2cpp_TypeInfo_var->static_fields)->set_alreadyShot_5((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_1 = ((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->get_needToSpawn_7();
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (L_2 == 0)
		{
			goto IL_002e;
		}
		if (L_2 == 1)
		{
			goto IL_0039;
		}
		if (L_2 == 2)
		{
			goto IL_0044;
		}
	}
	{
		goto IL_0055;
	}

IL_002e:
	{
		Spawner_SpawnLeftCowboy_m3571446914(__this, /*hidden argument*/NULL);
		goto IL_0055;
	}

IL_0039:
	{
		Spawner_SpawnRightCowboy_m1320007191(__this, /*hidden argument*/NULL);
		goto IL_0055;
	}

IL_0044:
	{
		Spawner_SpawnLeftCowboy_m3571446914(__this, /*hidden argument*/NULL);
		Spawner_SpawnRightCowboy_m1320007191(__this, /*hidden argument*/NULL);
		goto IL_0055;
	}

IL_0055:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_3 = GameStateManager_get_CowboysOnScene_m3965318281(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)2))))
		{
			goto IL_0066;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		GameStateManager_set_currentGameState_m1712983881(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
	}

IL_0066:
	{
		return;
	}
}
// System.Void Spawner::SpawnLeftCowboy()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1448170597_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1366199518_m3664764861_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisReSkinAnimation_t3053752670_m4260080765_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3634905883;
extern Il2CppCodeGenString* _stringLiteral3921959507;
extern const uint32_t Spawner_SpawnLeftCowboy_m3571446914_MetadataUsageId;
extern "C"  void Spawner_SpawnLeftCowboy_m3571446914 (Spawner_t534830648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Spawner_SpawnLeftCowboy_m3571446914_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ReSkinAnimation_t3053752670 * V_0 = NULL;
	{
		GameObject_t1366199518 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = GameObject_get_tag_m1425941094(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, _stringLiteral3634905883, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0047;
		}
	}
	{
		GameObject_t1366199518 * L_3 = __this->get_cowboyPrefab_2();
		GameObject_t1366199518 * L_4 = Object_Instantiate_TisGameObject_t1366199518_m3664764861(NULL /*static, unused*/, L_3, /*hidden argument*/Object_Instantiate_TisGameObject_t1366199518_m3664764861_MethodInfo_var);
		NullCheck(L_4);
		ReSkinAnimation_t3053752670 * L_5 = GameObject_GetComponent_TisReSkinAnimation_t3053752670_m4260080765(L_4, /*hidden argument*/GameObject_GetComponent_TisReSkinAnimation_t3053752670_m4260080765_MethodInfo_var);
		V_0 = L_5;
		ReSkinAnimation_t3053752670 * L_6 = V_0;
		int32_t L_7 = Random_Range_m694320887(NULL /*static, unused*/, 1, 8, /*hidden argument*/NULL);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t1448170597_il2cpp_TypeInfo_var, &L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3921959507, L_9, /*hidden argument*/NULL);
		NullCheck(L_6);
		L_6->set_spriteSheetName_2(L_10);
	}

IL_0047:
	{
		return;
	}
}
// System.Void Spawner::SpawnRightCowboy()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1448170597_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1366199518_m3664764861_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisReSkinAnimation_t3053752670_m4260080765_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1965745192;
extern Il2CppCodeGenString* _stringLiteral3921959507;
extern const uint32_t Spawner_SpawnRightCowboy_m1320007191_MetadataUsageId;
extern "C"  void Spawner_SpawnRightCowboy_m1320007191 (Spawner_t534830648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Spawner_SpawnRightCowboy_m1320007191_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ReSkinAnimation_t3053752670 * V_0 = NULL;
	{
		GameObject_t1366199518 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = GameObject_get_tag_m1425941094(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, _stringLiteral1965745192, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0047;
		}
	}
	{
		GameObject_t1366199518 * L_3 = __this->get_cowboyPrefab_2();
		GameObject_t1366199518 * L_4 = Object_Instantiate_TisGameObject_t1366199518_m3664764861(NULL /*static, unused*/, L_3, /*hidden argument*/Object_Instantiate_TisGameObject_t1366199518_m3664764861_MethodInfo_var);
		NullCheck(L_4);
		ReSkinAnimation_t3053752670 * L_5 = GameObject_GetComponent_TisReSkinAnimation_t3053752670_m4260080765(L_4, /*hidden argument*/GameObject_GetComponent_TisReSkinAnimation_t3053752670_m4260080765_MethodInfo_var);
		V_0 = L_5;
		ReSkinAnimation_t3053752670 * L_6 = V_0;
		int32_t L_7 = Random_Range_m694320887(NULL /*static, unused*/, 1, 8, /*hidden argument*/NULL);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t1448170597_il2cpp_TypeInfo_var, &L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3921959507, L_9, /*hidden argument*/NULL);
		NullCheck(L_6);
		L_6->set_spriteSheetName_2(L_10);
	}

IL_0047:
	{
		return;
	}
}
// System.Void StartGame::.ctor()
extern "C"  void StartGame__ctor_m4206793249 (StartGame_t415325182 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StartGame::Start()
extern TypeInfo* StartGame_t415325182_il2cpp_TypeInfo_var;
extern const uint32_t StartGame_Start_m2013778073_MetadataUsageId;
extern "C"  void StartGame_Start_m2013778073 (StartGame_t415325182 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartGame_Start_m2013778073_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t1366199518 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		((StartGame_t415325182_StaticFields*)StartGame_t415325182_il2cpp_TypeInfo_var->static_fields)->set_tapText_2(L_0);
		return;
	}
}
// System.Void StartGame::Update()
extern TypeInfo* GameStateManager_t1063875770_il2cpp_TypeInfo_var;
extern TypeInfo* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern TypeInfo* AudioClipU5BU5D_t3948543552_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t4173266137_il2cpp_TypeInfo_var;
extern TypeInfo* StartGame_t415325182_il2cpp_TypeInfo_var;
extern const uint32_t StartGame_Update_m1355775420_MetadataUsageId;
extern "C"  void StartGame_Update_m1355775420 (StartGame_t415325182 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartGame_Update_m1355775420_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_0 = GameStateManager_get_currentGameState_m2409036446(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0058;
		}
	}
	{
		bool L_1 = __this->get_musicStarted_4();
		if (L_1)
		{
			goto IL_0035;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManager_t654432262_il2cpp_TypeInfo_var);
		SoundManager_t654432262 * L_2 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_3();
		AudioClipU5BU5D_t3948543552* L_3 = ((AudioClipU5BU5D_t3948543552*)SZArrayNew(AudioClipU5BU5D_t3948543552_il2cpp_TypeInfo_var, (uint32_t)1));
		AudioClip_t3927647597 * L_4 = __this->get_startMusic_3();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (AudioClip_t3927647597 *)L_4);
		NullCheck(L_2);
		SoundManager_RandomizeSfx_m3482976376(L_2, L_3, /*hidden argument*/NULL);
		__this->set_musicStarted_4((bool)1);
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4173266137_il2cpp_TypeInfo_var);
		bool L_5 = Input_GetMouseButtonDown_m47917805(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0058;
		}
	}
	{
		GameObject_t1366199518 * L_6 = ((StartGame_t415325182_StaticFields*)StartGame_t415325182_il2cpp_TypeInfo_var->static_fields)->get_tapText_2();
		NullCheck(L_6);
		GameObject_SetActive_m2887581199(L_6, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		GameStateManager_set_currentGameState_m1712983881(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		__this->set_musicStarted_4((bool)0);
	}

IL_0058:
	{
		return;
	}
}
// System.Void StartGame::RestartGame()
extern TypeInfo* GameStateManager_t1063875770_il2cpp_TypeInfo_var;
extern TypeInfo* StartGame_t415325182_il2cpp_TypeInfo_var;
extern TypeInfo* Shot_t3441902592_il2cpp_TypeInfo_var;
extern const uint32_t StartGame_RestartGame_m870328564_MetadataUsageId;
extern "C"  void StartGame_RestartGame_m870328564 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StartGame_RestartGame_m870328564_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		GameStateManager_set_currentGameState_m1712983881(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->set_scoreLeft_5(0);
		((GameStateManager_t1063875770_StaticFields*)GameStateManager_t1063875770_il2cpp_TypeInfo_var->static_fields)->set_scoreRight_6(0);
		GameObject_t1366199518 * L_0 = ((StartGame_t415325182_StaticFields*)StartGame_t415325182_il2cpp_TypeInfo_var->static_fields)->get_tapText_2();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Shot_t3441902592_il2cpp_TypeInfo_var);
		((Shot_t3441902592_StaticFields*)Shot_t3441902592_il2cpp_TypeInfo_var->static_fields)->set_goToShotSoundPlayed_6((bool)0);
		return;
	}
}
// System.Void WinStateCowboyAnimation::.ctor()
extern "C"  void WinStateCowboyAnimation__ctor_m3646128773 (WinStateCowboyAnimation_t2747849222 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WinStateCowboyAnimation::.cctor()
extern "C"  void WinStateCowboyAnimation__cctor_m763032416 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void WinStateCowboyAnimation::Start()
extern const MethodInfo* Component_GetComponent_TisAnimationManager_t2328214095_m400137410_MethodInfo_var;
extern const uint32_t WinStateCowboyAnimation_Start_m3897844365_MetadataUsageId;
extern "C"  void WinStateCowboyAnimation_Start_m3897844365 (WinStateCowboyAnimation_t2747849222 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WinStateCowboyAnimation_Start_m3897844365_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AnimationManager_t2328214095 * L_0 = Component_GetComponent_TisAnimationManager_t2328214095_m400137410(__this, /*hidden argument*/Component_GetComponent_TisAnimationManager_t2328214095_m400137410_MethodInfo_var);
		__this->set_animator_2(L_0);
		return;
	}
}
// System.Void WinStateCowboyAnimation::Update()
extern TypeInfo* GameStateManager_t1063875770_il2cpp_TypeInfo_var;
extern TypeInfo* WinStateCowboyAnimation_t2747849222_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1020535556;
extern const uint32_t WinStateCowboyAnimation_Update_m2103924808_MetadataUsageId;
extern "C"  void WinStateCowboyAnimation_Update_m2103924808 (WinStateCowboyAnimation_t2747849222 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WinStateCowboyAnimation_Update_m2103924808_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameStateManager_t1063875770_il2cpp_TypeInfo_var);
		int32_t L_0 = GameStateManager_get_currentGameState_m2409036446(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)6))))
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WinStateCowboyAnimation_t2747849222_il2cpp_TypeInfo_var);
		bool L_1 = ((WinStateCowboyAnimation_t2747849222_StaticFields*)WinStateCowboyAnimation_t2747849222_il2cpp_TypeInfo_var->static_fields)->get_waitForWinStarted_3();
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		MonoBehaviour_StartCoroutine_m1399371129(__this, _stringLiteral1020535556, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(WinStateCowboyAnimation_t2747849222_il2cpp_TypeInfo_var);
		((WinStateCowboyAnimation_t2747849222_StaticFields*)WinStateCowboyAnimation_t2747849222_il2cpp_TypeInfo_var->static_fields)->set_waitForWinStarted_3((bool)1);
	}

IL_0027:
	{
		return;
	}
}
// System.Collections.IEnumerator WinStateCowboyAnimation::WaitForWin()
extern TypeInfo* U3CWaitForWinU3Ec__Iterator4_t1495125349_il2cpp_TypeInfo_var;
extern const uint32_t WinStateCowboyAnimation_WaitForWin_m3463609159_MetadataUsageId;
extern "C"  Il2CppObject * WinStateCowboyAnimation_WaitForWin_m3463609159 (WinStateCowboyAnimation_t2747849222 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WinStateCowboyAnimation_WaitForWin_m3463609159_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CWaitForWinU3Ec__Iterator4_t1495125349 * V_0 = NULL;
	{
		U3CWaitForWinU3Ec__Iterator4_t1495125349 * L_0 = (U3CWaitForWinU3Ec__Iterator4_t1495125349 *)il2cpp_codegen_object_new(U3CWaitForWinU3Ec__Iterator4_t1495125349_il2cpp_TypeInfo_var);
		U3CWaitForWinU3Ec__Iterator4__ctor_m722025945(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitForWinU3Ec__Iterator4_t1495125349 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CWaitForWinU3Ec__Iterator4_t1495125349 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator WinStateCowboyAnimation::WaitForRestart()
extern TypeInfo* U3CWaitForRestartU3Ec__Iterator5_t2340398903_il2cpp_TypeInfo_var;
extern const uint32_t WinStateCowboyAnimation_WaitForRestart_m1772306694_MetadataUsageId;
extern "C"  Il2CppObject * WinStateCowboyAnimation_WaitForRestart_m1772306694 (WinStateCowboyAnimation_t2747849222 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WinStateCowboyAnimation_WaitForRestart_m1772306694_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CWaitForRestartU3Ec__Iterator5_t2340398903 * V_0 = NULL;
	{
		U3CWaitForRestartU3Ec__Iterator5_t2340398903 * L_0 = (U3CWaitForRestartU3Ec__Iterator5_t2340398903 *)il2cpp_codegen_object_new(U3CWaitForRestartU3Ec__Iterator5_t2340398903_il2cpp_TypeInfo_var);
		U3CWaitForRestartU3Ec__Iterator5__ctor_m1666317843(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitForRestartU3Ec__Iterator5_t2340398903 * L_1 = V_0;
		return L_1;
	}
}
// System.Void WinStateCowboyAnimation/<WaitForRestart>c__Iterator5::.ctor()
extern "C"  void U3CWaitForRestartU3Ec__Iterator5__ctor_m1666317843 (U3CWaitForRestartU3Ec__Iterator5_t2340398903 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object WinStateCowboyAnimation/<WaitForRestart>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForRestartU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m944195257 (U3CWaitForRestartU3Ec__Iterator5_t2340398903 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object WinStateCowboyAnimation/<WaitForRestart>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForRestartU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m587967233 (U3CWaitForRestartU3Ec__Iterator5_t2340398903 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean WinStateCowboyAnimation/<WaitForRestart>c__Iterator5::MoveNext()
extern TypeInfo* WaitForSeconds_t1717981302_il2cpp_TypeInfo_var;
extern TypeInfo* WinStateCowboyAnimation_t2747849222_il2cpp_TypeInfo_var;
extern const uint32_t U3CWaitForRestartU3Ec__Iterator5_MoveNext_m986672837_MetadataUsageId;
extern "C"  bool U3CWaitForRestartU3Ec__Iterator5_MoveNext_m986672837 (U3CWaitForRestartU3Ec__Iterator5_t2340398903 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitForRestartU3Ec__Iterator5_MoveNext_m986672837_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003d;
		}
	}
	{
		goto IL_004f;
	}

IL_0021:
	{
		WaitForSeconds_t1717981302 * L_2 = (WaitForSeconds_t1717981302 *)il2cpp_codegen_object_new(WaitForSeconds_t1717981302_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_2, (3.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		__this->set_U24PC_0(1);
		goto IL_0051;
	}

IL_003d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(WinStateCowboyAnimation_t2747849222_il2cpp_TypeInfo_var);
		((WinStateCowboyAnimation_t2747849222_StaticFields*)WinStateCowboyAnimation_t2747849222_il2cpp_TypeInfo_var->static_fields)->set_waitForWinStarted_3((bool)0);
		StartGame_RestartGame_m870328564(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U24PC_0((-1));
	}

IL_004f:
	{
		return (bool)0;
	}

IL_0051:
	{
		return (bool)1;
	}
	// Dead block : IL_0053: ldloc.1
}
// System.Void WinStateCowboyAnimation/<WaitForRestart>c__Iterator5::Dispose()
extern "C"  void U3CWaitForRestartU3Ec__Iterator5_Dispose_m3331584906 (U3CWaitForRestartU3Ec__Iterator5_t2340398903 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void WinStateCowboyAnimation/<WaitForRestart>c__Iterator5::Reset()
extern TypeInfo* NotSupportedException_t3178859535_il2cpp_TypeInfo_var;
extern const uint32_t U3CWaitForRestartU3Ec__Iterator5_Reset_m874998788_MetadataUsageId;
extern "C"  void U3CWaitForRestartU3Ec__Iterator5_Reset_m874998788 (U3CWaitForRestartU3Ec__Iterator5_t2340398903 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitForRestartU3Ec__Iterator5_Reset_m874998788_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t3178859535 * L_0 = (NotSupportedException_t3178859535 *)il2cpp_codegen_object_new(NotSupportedException_t3178859535_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void WinStateCowboyAnimation/<WaitForWin>c__Iterator4::.ctor()
extern "C"  void U3CWaitForWinU3Ec__Iterator4__ctor_m722025945 (U3CWaitForWinU3Ec__Iterator4_t1495125349 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object WinStateCowboyAnimation/<WaitForWin>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForWinU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2828893075 (U3CWaitForWinU3Ec__Iterator4_t1495125349 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object WinStateCowboyAnimation/<WaitForWin>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForWinU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1312678731 (U3CWaitForWinU3Ec__Iterator4_t1495125349 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean WinStateCowboyAnimation/<WaitForWin>c__Iterator4::MoveNext()
extern TypeInfo* WaitForSeconds_t1717981302_il2cpp_TypeInfo_var;
extern TypeInfo* SoundManager_t654432262_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral140881603;
extern const uint32_t U3CWaitForWinU3Ec__Iterator4_MoveNext_m4120676603_MetadataUsageId;
extern "C"  bool U3CWaitForWinU3Ec__Iterator4_MoveNext_m4120676603 (U3CWaitForWinU3Ec__Iterator4_t1495125349 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitForWinU3Ec__Iterator4_MoveNext_m4120676603_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003d;
		}
	}
	{
		goto IL_006f;
	}

IL_0021:
	{
		WaitForSeconds_t1717981302 * L_2 = (WaitForSeconds_t1717981302 *)il2cpp_codegen_object_new(WaitForSeconds_t1717981302_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_2, (2.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		__this->set_U24PC_0(1);
		goto IL_0071;
	}

IL_003d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SoundManager_t654432262_il2cpp_TypeInfo_var);
		SoundManager_t654432262 * L_3 = ((SoundManager_t654432262_StaticFields*)SoundManager_t654432262_il2cpp_TypeInfo_var->static_fields)->get_instance_3();
		NullCheck(L_3);
		SoundManager_PlayWinStateMusic_m2740338453(L_3, /*hidden argument*/NULL);
		WinStateCowboyAnimation_t2747849222 * L_4 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_4);
		AnimationManager_t2328214095 * L_5 = L_4->get_animator_2();
		NullCheck(L_5);
		AnimationManager_Win_m4182162318(L_5, /*hidden argument*/NULL);
		WinStateCowboyAnimation_t2747849222 * L_6 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_6);
		MonoBehaviour_StartCoroutine_m1399371129(L_6, _stringLiteral140881603, /*hidden argument*/NULL);
		__this->set_U24PC_0((-1));
	}

IL_006f:
	{
		return (bool)0;
	}

IL_0071:
	{
		return (bool)1;
	}
	// Dead block : IL_0073: ldloc.1
}
// System.Void WinStateCowboyAnimation/<WaitForWin>c__Iterator4::Dispose()
extern "C"  void U3CWaitForWinU3Ec__Iterator4_Dispose_m257474778 (U3CWaitForWinU3Ec__Iterator4_t1495125349 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void WinStateCowboyAnimation/<WaitForWin>c__Iterator4::Reset()
extern TypeInfo* NotSupportedException_t3178859535_il2cpp_TypeInfo_var;
extern const uint32_t U3CWaitForWinU3Ec__Iterator4_Reset_m3947890464_MetadataUsageId;
extern "C"  void U3CWaitForWinU3Ec__Iterator4_Reset_m3947890464 (U3CWaitForWinU3Ec__Iterator4_t1495125349 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitForWinU3Ec__Iterator4_Reset_m3947890464_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t3178859535 * L_0 = (NotSupportedException_t3178859535 *)il2cpp_codegen_object_new(NotSupportedException_t3178859535_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
