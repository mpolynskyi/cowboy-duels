﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameStateManager
struct GameStateManager_t1063875770;

#include "codegen/il2cpp-codegen.h"

// System.Void GameStateManager::.ctor()
extern "C"  void GameStateManager__ctor_m189335085 (GameStateManager_t1063875770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameStateManager::.cctor()
extern "C"  void GameStateManager__cctor_m623853036 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameStateManager::get_currentGameState()
extern "C"  int32_t GameStateManager_get_currentGameState_m2409036446 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameStateManager::set_currentGameState(System.Int32)
extern "C"  void GameStateManager_set_currentGameState_m1712983881 (Il2CppObject * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameStateManager::get_RandomDelay()
extern "C"  int32_t GameStateManager_get_RandomDelay_m3340467374 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameStateManager::set_RandomDelay(System.Int32)
extern "C"  void GameStateManager_set_RandomDelay_m475923579 (Il2CppObject * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameStateManager::get_CowboysOnScene()
extern "C"  int32_t GameStateManager_get_CowboysOnScene_m3965318281 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameStateManager::ResetRandomDelay()
extern "C"  void GameStateManager_ResetRandomDelay_m2974787186 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
