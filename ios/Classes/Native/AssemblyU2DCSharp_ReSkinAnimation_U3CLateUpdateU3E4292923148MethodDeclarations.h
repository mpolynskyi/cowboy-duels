﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReSkinAnimation/<LateUpdate>c__AnonStorey6
struct U3CLateUpdateU3Ec__AnonStorey6_t4292923148;
// UnityEngine.Sprite
struct Sprite_t1118776648;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Sprite1118776648.h"

// System.Void ReSkinAnimation/<LateUpdate>c__AnonStorey6::.ctor()
extern "C"  void U3CLateUpdateU3Ec__AnonStorey6__ctor_m2047503238 (U3CLateUpdateU3Ec__AnonStorey6_t4292923148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReSkinAnimation/<LateUpdate>c__AnonStorey6::<>m__0(UnityEngine.Sprite)
extern "C"  bool U3CLateUpdateU3Ec__AnonStorey6_U3CU3Em__0_m4006040457 (U3CLateUpdateU3Ec__AnonStorey6_t4292923148 * __this, Sprite_t1118776648 * ___item, const MethodInfo* method) IL2CPP_METHOD_ATTR;
