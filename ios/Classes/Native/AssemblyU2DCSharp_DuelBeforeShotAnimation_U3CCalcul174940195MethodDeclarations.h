﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DuelBeforeShotAnimation/<CalculateAnimationLength>c__Iterator0
struct U3CCalculateAnimationLengthU3Ec__Iterator0_t174940195;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void DuelBeforeShotAnimation/<CalculateAnimationLength>c__Iterator0::.ctor()
extern "C"  void U3CCalculateAnimationLengthU3Ec__Iterator0__ctor_m1257055024 (U3CCalculateAnimationLengthU3Ec__Iterator0_t174940195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DuelBeforeShotAnimation/<CalculateAnimationLength>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCalculateAnimationLengthU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m597024660 (U3CCalculateAnimationLengthU3Ec__Iterator0_t174940195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DuelBeforeShotAnimation/<CalculateAnimationLength>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCalculateAnimationLengthU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m871187340 (U3CCalculateAnimationLengthU3Ec__Iterator0_t174940195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DuelBeforeShotAnimation/<CalculateAnimationLength>c__Iterator0::MoveNext()
extern "C"  bool U3CCalculateAnimationLengthU3Ec__Iterator0_MoveNext_m2947384468 (U3CCalculateAnimationLengthU3Ec__Iterator0_t174940195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DuelBeforeShotAnimation/<CalculateAnimationLength>c__Iterator0::Dispose()
extern "C"  void U3CCalculateAnimationLengthU3Ec__Iterator0_Dispose_m2752935565 (U3CCalculateAnimationLengthU3Ec__Iterator0_t174940195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DuelBeforeShotAnimation/<CalculateAnimationLength>c__Iterator0::Reset()
extern "C"  void U3CCalculateAnimationLengthU3Ec__Iterator0_Reset_m424804167 (U3CCalculateAnimationLengthU3Ec__Iterator0_t174940195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
