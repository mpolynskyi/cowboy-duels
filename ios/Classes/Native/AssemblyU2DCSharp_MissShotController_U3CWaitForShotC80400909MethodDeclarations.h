﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MissShotController/<WaitForShotCorutine>c__Iterator1
struct U3CWaitForShotCorutineU3Ec__Iterator1_t80400909;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MissShotController/<WaitForShotCorutine>c__Iterator1::.ctor()
extern "C"  void U3CWaitForShotCorutineU3Ec__Iterator1__ctor_m3051622455 (U3CWaitForShotCorutineU3Ec__Iterator1_t80400909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MissShotController/<WaitForShotCorutine>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForShotCorutineU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2201498381 (U3CWaitForShotCorutineU3Ec__Iterator1_t80400909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MissShotController/<WaitForShotCorutine>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForShotCorutineU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m774118165 (U3CWaitForShotCorutineU3Ec__Iterator1_t80400909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MissShotController/<WaitForShotCorutine>c__Iterator1::MoveNext()
extern "C"  bool U3CWaitForShotCorutineU3Ec__Iterator1_MoveNext_m1566210489 (U3CWaitForShotCorutineU3Ec__Iterator1_t80400909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissShotController/<WaitForShotCorutine>c__Iterator1::Dispose()
extern "C"  void U3CWaitForShotCorutineU3Ec__Iterator1_Dispose_m3097202252 (U3CWaitForShotCorutineU3Ec__Iterator1_t80400909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissShotController/<WaitForShotCorutine>c__Iterator1::Reset()
extern "C"  void U3CWaitForShotCorutineU3Ec__Iterator1_Reset_m440723094 (U3CWaitForShotCorutineU3Ec__Iterator1_t80400909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
