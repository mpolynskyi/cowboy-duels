﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;



public class Shot : MonoBehaviour {
    AnimationManager animator;
    GameObject playerLeft;
    GameObject playerRight;
    public static bool alreadyShot = false;
    public static bool goToShotSoundPlayed = false;

    void Start()
    {
        animator = GetComponent<AnimationManager>();
    }

    void Update()
    {
        if (GameStateManager.currentGameState == (int)GameStateManager.DuelStates.cowboys_aim_guns)
        {
            if (playerLeft == null) {
                playerLeft = GameObject.FindWithTag("PlayerLeft");
            }
            if (playerRight == null) {
                playerRight = GameObject.FindWithTag("PlayerRight");
            }

            if (!goToShotSoundPlayed)
            {
                SoundManager.instance.PlayFire();
                goToShotSoundPlayed = true;
            }

            if (Input.GetMouseButtonDown(0))
            {
                if (transform.position.x < 0 & Input.mousePosition.x < Screen.width / 2)   //tap from left
                {
                    
                    if (!alreadyShot)
                    {
                        //left cowboy shot
                        alreadyShot = true;
                        GameStateManager.scoreLeft++;
                        animator.Shot();
                        SoundManager.instance.PlaySuicideSound();
                        var playerRightRenderer = playerRight.GetComponent<SpriteRenderer>();
                        playerRightRenderer.flipX = false;  // if not do it the "Bang!"sprite will be fliped 
                        playerRight.SendMessage("Bang");
                        GameStateManager.needToSpawn = (int)GameStateManager.WhoToSpawn.right;
                        if (GameStateManager.scoreLeft == GameStateManager.maxScore || GameStateManager.scoreRight == GameStateManager.maxScore)
                        {
                            GameStateManager.currentGameState = (int)GameStateManager.DuelStates.end_game;
                            //GameStateManager.needToSpawn = (int)GameStateManager.WhoToSpawn.both;
                        }
                        else
                        {
                            StartCoroutine("WaitForSpawnNewCowboy");
                            GameStateManager.ResetRandomDelay();
                        }
                    }
                    
                }
                if (transform.position.x > 0 & Input.mousePosition.x > Screen.width / 2)   //tap from right
                {

                    if (!alreadyShot)
                    {
                        //right cowboy shot
                        alreadyShot = true;
                        GameStateManager.scoreRight++;
                        animator.Shot();
                        SoundManager.instance.PlaySuicideSound();
                        playerLeft.SendMessage("Bang");
                        GameStateManager.needToSpawn = (int)GameStateManager.WhoToSpawn.left;
                        if (GameStateManager.scoreRight == GameStateManager.maxScore || GameStateManager.scoreLeft == GameStateManager.maxScore)
                        {
                            GameStateManager.currentGameState = (int)GameStateManager.DuelStates.end_game;
                            //GameStateManager.needToSpawn = (int)GameStateManager.WhoToSpawn.both;
                        }
                        else
                        {
                            StartCoroutine("WaitForSpawnNewCowboy");
                            GameStateManager.ResetRandomDelay();
                        }
                        
                    }
                }
            }
        }
    }

    IEnumerator WaitForSpawnNewCowboy()
    {
        yield return new WaitForSeconds(3);
        goToShotSoundPlayed = false;
        GameStateManager.currentGameState = (int)GameStateManager.DuelStates.spawn_cowboy;
    }

}




