﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WinStateCowboyAnimation/<WaitForWin>c__Iterator4
struct U3CWaitForWinU3Ec__Iterator4_t1495125349;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void WinStateCowboyAnimation/<WaitForWin>c__Iterator4::.ctor()
extern "C"  void U3CWaitForWinU3Ec__Iterator4__ctor_m722025945 (U3CWaitForWinU3Ec__Iterator4_t1495125349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WinStateCowboyAnimation/<WaitForWin>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForWinU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2828893075 (U3CWaitForWinU3Ec__Iterator4_t1495125349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WinStateCowboyAnimation/<WaitForWin>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForWinU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1312678731 (U3CWaitForWinU3Ec__Iterator4_t1495125349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WinStateCowboyAnimation/<WaitForWin>c__Iterator4::MoveNext()
extern "C"  bool U3CWaitForWinU3Ec__Iterator4_MoveNext_m4120676603 (U3CWaitForWinU3Ec__Iterator4_t1495125349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WinStateCowboyAnimation/<WaitForWin>c__Iterator4::Dispose()
extern "C"  void U3CWaitForWinU3Ec__Iterator4_Dispose_m257474778 (U3CWaitForWinU3Ec__Iterator4_t1495125349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WinStateCowboyAnimation/<WaitForWin>c__Iterator4::Reset()
extern "C"  void U3CWaitForWinU3Ec__Iterator4_Reset_m3947890464 (U3CWaitForWinU3Ec__Iterator4_t1495125349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
