﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animator
struct Animator_t2442207934;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimationManager
struct  AnimationManager_t2328214095  : public MonoBehaviour_t774292115
{
public:
	// UnityEngine.Animator AnimationManager::animator
	Animator_t2442207934 * ___animator_2;

public:
	inline static int32_t get_offset_of_animator_2() { return static_cast<int32_t>(offsetof(AnimationManager_t2328214095, ___animator_2)); }
	inline Animator_t2442207934 * get_animator_2() const { return ___animator_2; }
	inline Animator_t2442207934 ** get_address_of_animator_2() { return &___animator_2; }
	inline void set_animator_2(Animator_t2442207934 * value)
	{
		___animator_2 = value;
		Il2CppCodeGenWriteBarrier(&___animator_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
