﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Death
struct Death_t2883266902;

#include "codegen/il2cpp-codegen.h"

// System.Void Death::.ctor()
extern "C"  void Death__ctor_m2539501311 (Death_t2883266902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Death::Die()
extern "C"  void Death_Die_m166476791 (Death_t2883266902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Death::Disable()
extern "C"  void Death_Disable_m287603695 (Death_t2883266902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
