﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioSource
struct AudioSource_t585923902;
// SoundManager
struct SoundManager_t654432262;
// UnityEngine.AudioClip
struct AudioClip_t3927647597;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundManager
struct  SoundManager_t654432262  : public MonoBehaviour_t774292115
{
public:
	// UnityEngine.AudioSource SoundManager::efxSource
	AudioSource_t585923902 * ___efxSource_2;
	// System.Single SoundManager::lowPitchRange
	float ___lowPitchRange_4;
	// System.Single SoundManager::highPitchRange
	float ___highPitchRange_5;
	// UnityEngine.AudioClip SoundManager::cowboys_move_to_center
	AudioClip_t3927647597 * ___cowboys_move_to_center_6;
	// UnityEngine.AudioClip SoundManager::suicideSound
	AudioClip_t3927647597 * ___suicideSound_7;
	// UnityEngine.AudioClip SoundManager::fireSound
	AudioClip_t3927647597 * ___fireSound_8;
	// UnityEngine.AudioClip SoundManager::winMusic
	AudioClip_t3927647597 * ___winMusic_9;

public:
	inline static int32_t get_offset_of_efxSource_2() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___efxSource_2)); }
	inline AudioSource_t585923902 * get_efxSource_2() const { return ___efxSource_2; }
	inline AudioSource_t585923902 ** get_address_of_efxSource_2() { return &___efxSource_2; }
	inline void set_efxSource_2(AudioSource_t585923902 * value)
	{
		___efxSource_2 = value;
		Il2CppCodeGenWriteBarrier(&___efxSource_2, value);
	}

	inline static int32_t get_offset_of_lowPitchRange_4() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___lowPitchRange_4)); }
	inline float get_lowPitchRange_4() const { return ___lowPitchRange_4; }
	inline float* get_address_of_lowPitchRange_4() { return &___lowPitchRange_4; }
	inline void set_lowPitchRange_4(float value)
	{
		___lowPitchRange_4 = value;
	}

	inline static int32_t get_offset_of_highPitchRange_5() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___highPitchRange_5)); }
	inline float get_highPitchRange_5() const { return ___highPitchRange_5; }
	inline float* get_address_of_highPitchRange_5() { return &___highPitchRange_5; }
	inline void set_highPitchRange_5(float value)
	{
		___highPitchRange_5 = value;
	}

	inline static int32_t get_offset_of_cowboys_move_to_center_6() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___cowboys_move_to_center_6)); }
	inline AudioClip_t3927647597 * get_cowboys_move_to_center_6() const { return ___cowboys_move_to_center_6; }
	inline AudioClip_t3927647597 ** get_address_of_cowboys_move_to_center_6() { return &___cowboys_move_to_center_6; }
	inline void set_cowboys_move_to_center_6(AudioClip_t3927647597 * value)
	{
		___cowboys_move_to_center_6 = value;
		Il2CppCodeGenWriteBarrier(&___cowboys_move_to_center_6, value);
	}

	inline static int32_t get_offset_of_suicideSound_7() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___suicideSound_7)); }
	inline AudioClip_t3927647597 * get_suicideSound_7() const { return ___suicideSound_7; }
	inline AudioClip_t3927647597 ** get_address_of_suicideSound_7() { return &___suicideSound_7; }
	inline void set_suicideSound_7(AudioClip_t3927647597 * value)
	{
		___suicideSound_7 = value;
		Il2CppCodeGenWriteBarrier(&___suicideSound_7, value);
	}

	inline static int32_t get_offset_of_fireSound_8() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___fireSound_8)); }
	inline AudioClip_t3927647597 * get_fireSound_8() const { return ___fireSound_8; }
	inline AudioClip_t3927647597 ** get_address_of_fireSound_8() { return &___fireSound_8; }
	inline void set_fireSound_8(AudioClip_t3927647597 * value)
	{
		___fireSound_8 = value;
		Il2CppCodeGenWriteBarrier(&___fireSound_8, value);
	}

	inline static int32_t get_offset_of_winMusic_9() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___winMusic_9)); }
	inline AudioClip_t3927647597 * get_winMusic_9() const { return ___winMusic_9; }
	inline AudioClip_t3927647597 ** get_address_of_winMusic_9() { return &___winMusic_9; }
	inline void set_winMusic_9(AudioClip_t3927647597 * value)
	{
		___winMusic_9 = value;
		Il2CppCodeGenWriteBarrier(&___winMusic_9, value);
	}
};

struct SoundManager_t654432262_StaticFields
{
public:
	// SoundManager SoundManager::instance
	SoundManager_t654432262 * ___instance_3;

public:
	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(SoundManager_t654432262_StaticFields, ___instance_3)); }
	inline SoundManager_t654432262 * get_instance_3() const { return ___instance_3; }
	inline SoundManager_t654432262 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(SoundManager_t654432262 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier(&___instance_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
