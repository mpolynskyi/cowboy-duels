﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AnimationManager
struct AnimationManager_t2328214095;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"
#include "UnityEngine_UnityEngine_Vector2465617798.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PrepareToDuel
struct  PrepareToDuel_t908926924  : public MonoBehaviour_t774292115
{
public:
	// System.Single PrepareToDuel::pixelsFromCenter
	float ___pixelsFromCenter_2;
	// System.Single PrepareToDuel::speed
	float ___speed_3;
	// UnityEngine.Vector2 PrepareToDuel::sideTo
	Vector2_t465617798  ___sideTo_4;
	// System.Single PrepareToDuel::stopPointX
	float ___stopPointX_5;
	// AnimationManager PrepareToDuel::animator
	AnimationManager_t2328214095 * ___animator_6;
	// System.Boolean PrepareToDuel::cowboys_move_to_center_started
	bool ___cowboys_move_to_center_started_7;
	// System.Boolean PrepareToDuel::_playerStoped
	bool ____playerStoped_9;

public:
	inline static int32_t get_offset_of_pixelsFromCenter_2() { return static_cast<int32_t>(offsetof(PrepareToDuel_t908926924, ___pixelsFromCenter_2)); }
	inline float get_pixelsFromCenter_2() const { return ___pixelsFromCenter_2; }
	inline float* get_address_of_pixelsFromCenter_2() { return &___pixelsFromCenter_2; }
	inline void set_pixelsFromCenter_2(float value)
	{
		___pixelsFromCenter_2 = value;
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(PrepareToDuel_t908926924, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_sideTo_4() { return static_cast<int32_t>(offsetof(PrepareToDuel_t908926924, ___sideTo_4)); }
	inline Vector2_t465617798  get_sideTo_4() const { return ___sideTo_4; }
	inline Vector2_t465617798 * get_address_of_sideTo_4() { return &___sideTo_4; }
	inline void set_sideTo_4(Vector2_t465617798  value)
	{
		___sideTo_4 = value;
	}

	inline static int32_t get_offset_of_stopPointX_5() { return static_cast<int32_t>(offsetof(PrepareToDuel_t908926924, ___stopPointX_5)); }
	inline float get_stopPointX_5() const { return ___stopPointX_5; }
	inline float* get_address_of_stopPointX_5() { return &___stopPointX_5; }
	inline void set_stopPointX_5(float value)
	{
		___stopPointX_5 = value;
	}

	inline static int32_t get_offset_of_animator_6() { return static_cast<int32_t>(offsetof(PrepareToDuel_t908926924, ___animator_6)); }
	inline AnimationManager_t2328214095 * get_animator_6() const { return ___animator_6; }
	inline AnimationManager_t2328214095 ** get_address_of_animator_6() { return &___animator_6; }
	inline void set_animator_6(AnimationManager_t2328214095 * value)
	{
		___animator_6 = value;
		Il2CppCodeGenWriteBarrier(&___animator_6, value);
	}

	inline static int32_t get_offset_of_cowboys_move_to_center_started_7() { return static_cast<int32_t>(offsetof(PrepareToDuel_t908926924, ___cowboys_move_to_center_started_7)); }
	inline bool get_cowboys_move_to_center_started_7() const { return ___cowboys_move_to_center_started_7; }
	inline bool* get_address_of_cowboys_move_to_center_started_7() { return &___cowboys_move_to_center_started_7; }
	inline void set_cowboys_move_to_center_started_7(bool value)
	{
		___cowboys_move_to_center_started_7 = value;
	}

	inline static int32_t get_offset_of__playerStoped_9() { return static_cast<int32_t>(offsetof(PrepareToDuel_t908926924, ____playerStoped_9)); }
	inline bool get__playerStoped_9() const { return ____playerStoped_9; }
	inline bool* get_address_of__playerStoped_9() { return &____playerStoped_9; }
	inline void set__playerStoped_9(bool value)
	{
		____playerStoped_9 = value;
	}
};

struct PrepareToDuel_t908926924_StaticFields
{
public:
	// System.Boolean PrepareToDuel::playerStopedController
	bool ___playerStopedController_8;

public:
	inline static int32_t get_offset_of_playerStopedController_8() { return static_cast<int32_t>(offsetof(PrepareToDuel_t908926924_StaticFields, ___playerStopedController_8)); }
	inline bool get_playerStopedController_8() const { return ___playerStopedController_8; }
	inline bool* get_address_of_playerStopedController_8() { return &___playerStopedController_8; }
	inline void set_playerStopedController_8(bool value)
	{
		___playerStopedController_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
