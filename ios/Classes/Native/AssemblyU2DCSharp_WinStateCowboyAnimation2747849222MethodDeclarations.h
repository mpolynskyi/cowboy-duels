﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WinStateCowboyAnimation
struct WinStateCowboyAnimation_t2747849222;
// System.Collections.IEnumerator
struct IEnumerator_t3037427797;

#include "codegen/il2cpp-codegen.h"

// System.Void WinStateCowboyAnimation::.ctor()
extern "C"  void WinStateCowboyAnimation__ctor_m3646128773 (WinStateCowboyAnimation_t2747849222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WinStateCowboyAnimation::.cctor()
extern "C"  void WinStateCowboyAnimation__cctor_m763032416 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WinStateCowboyAnimation::Start()
extern "C"  void WinStateCowboyAnimation_Start_m3897844365 (WinStateCowboyAnimation_t2747849222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WinStateCowboyAnimation::Update()
extern "C"  void WinStateCowboyAnimation_Update_m2103924808 (WinStateCowboyAnimation_t2747849222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator WinStateCowboyAnimation::WaitForWin()
extern "C"  Il2CppObject * WinStateCowboyAnimation_WaitForWin_m3463609159 (WinStateCowboyAnimation_t2747849222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator WinStateCowboyAnimation::WaitForRestart()
extern "C"  Il2CppObject * WinStateCowboyAnimation_WaitForRestart_m1772306694 (WinStateCowboyAnimation_t2747849222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
