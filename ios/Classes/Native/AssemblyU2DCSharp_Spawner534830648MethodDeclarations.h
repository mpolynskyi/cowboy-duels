﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Spawner
struct Spawner_t534830648;

#include "codegen/il2cpp-codegen.h"

// System.Void Spawner::.ctor()
extern "C"  void Spawner__ctor_m1860016179 (Spawner_t534830648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spawner::.cctor()
extern "C"  void Spawner__cctor_m869600462 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spawner::Update()
extern "C"  void Spawner_Update_m1485478966 (Spawner_t534830648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spawner::SpawnLeftCowboy()
extern "C"  void Spawner_SpawnLeftCowboy_m3571446914 (Spawner_t534830648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Spawner::SpawnRightCowboy()
extern "C"  void Spawner_SpawnRightCowboy_m1320007191 (Spawner_t534830648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
