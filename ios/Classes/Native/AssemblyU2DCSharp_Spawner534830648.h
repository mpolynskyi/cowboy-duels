﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1366199518;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spawner
struct  Spawner_t534830648  : public MonoBehaviour_t774292115
{
public:
	// UnityEngine.GameObject Spawner::cowboyPrefab
	GameObject_t1366199518 * ___cowboyPrefab_2;

public:
	inline static int32_t get_offset_of_cowboyPrefab_2() { return static_cast<int32_t>(offsetof(Spawner_t534830648, ___cowboyPrefab_2)); }
	inline GameObject_t1366199518 * get_cowboyPrefab_2() const { return ___cowboyPrefab_2; }
	inline GameObject_t1366199518 ** get_address_of_cowboyPrefab_2() { return &___cowboyPrefab_2; }
	inline void set_cowboyPrefab_2(GameObject_t1366199518 * value)
	{
		___cowboyPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___cowboyPrefab_2, value);
	}
};

struct Spawner_t534830648_StaticFields
{
public:
	// System.Int32 Spawner::cowboysSpawned
	int32_t ___cowboysSpawned_3;

public:
	inline static int32_t get_offset_of_cowboysSpawned_3() { return static_cast<int32_t>(offsetof(Spawner_t534830648_StaticFields, ___cowboysSpawned_3)); }
	inline int32_t get_cowboysSpawned_3() const { return ___cowboysSpawned_3; }
	inline int32_t* get_address_of_cowboysSpawned_3() { return &___cowboysSpawned_3; }
	inline void set_cowboysSpawned_3(int32_t value)
	{
		___cowboysSpawned_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
