﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t2914288752;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReSkinAnimation
struct  ReSkinAnimation_t3053752670  : public MonoBehaviour_t774292115
{
public:
	// System.String ReSkinAnimation::spriteSheetName
	String_t* ___spriteSheetName_2;
	// UnityEngine.BoxCollider2D ReSkinAnimation::dynamicCollider
	BoxCollider2D_t2914288752 * ___dynamicCollider_3;

public:
	inline static int32_t get_offset_of_spriteSheetName_2() { return static_cast<int32_t>(offsetof(ReSkinAnimation_t3053752670, ___spriteSheetName_2)); }
	inline String_t* get_spriteSheetName_2() const { return ___spriteSheetName_2; }
	inline String_t** get_address_of_spriteSheetName_2() { return &___spriteSheetName_2; }
	inline void set_spriteSheetName_2(String_t* value)
	{
		___spriteSheetName_2 = value;
		Il2CppCodeGenWriteBarrier(&___spriteSheetName_2, value);
	}

	inline static int32_t get_offset_of_dynamicCollider_3() { return static_cast<int32_t>(offsetof(ReSkinAnimation_t3053752670, ___dynamicCollider_3)); }
	inline BoxCollider2D_t2914288752 * get_dynamicCollider_3() const { return ___dynamicCollider_3; }
	inline BoxCollider2D_t2914288752 ** get_address_of_dynamicCollider_3() { return &___dynamicCollider_3; }
	inline void set_dynamicCollider_3(BoxCollider2D_t2914288752 * value)
	{
		___dynamicCollider_3 = value;
		Il2CppCodeGenWriteBarrier(&___dynamicCollider_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
