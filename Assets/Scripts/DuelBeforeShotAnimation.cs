﻿using UnityEngine;
using System.Collections;

public class DuelBeforeShotAnimation : MonoBehaviour {
    private static bool waitForShotStarted = false;
    AnimationManager animator;
    public static float bangAnimationLength = 0;
    private static IEnumerator waitForShot;
    GameObject missShotController;
    GameObject playerLeft;
    GameObject playerRight;
    public static bool suicide = false;

    void Start ()
    {
        animator = GetComponent<AnimationManager>();
        missShotController = GameObject.FindWithTag("MissShotController");
        
    }

    void Update () {

        if (playerRight == null)  //I need it to flip bang animation
        {
            playerRight = GameObject.FindWithTag("PlayerRight");
        }


        if (GameStateManager.currentGameState == (int)GameStateManager.DuelStates.cowboys_ready)
        {
            

            if (Input.GetMouseButtonDown(0))
            {
                if (transform.position.x < 0 & Input.mousePosition.x < Screen.width / 2 & !suicide)  //tap from left
                {
                    suicide = true;
                    animator.Bang();
                    GameStateManager.needToSpawn = (int)GameStateManager.WhoToSpawn.left;
                    GameStateManager.scoreRight++;
                    SoundManager.instance.PlaySuicideSound(); // send message to play bang sound
                    StartCoroutine("CalculateAnimationLength");
                    //Если хоть один ковбой остановился - запускается корутина WaitForShot() которая меняет GameStateManager.currentGameState. Нужно чтобы не менялся стейт если один ковбой себя подорвал
                }
                if (transform.position.x > 0 & Input.mousePosition.x > Screen.width / 2 & !suicide)  //tap from left
                {
                    suicide = true;
                    var playerRightRenderer = playerRight.GetComponent<SpriteRenderer>();
                    playerRightRenderer.flipX = false;
                    animator.Bang();
                    GameStateManager.needToSpawn = (int)GameStateManager.WhoToSpawn.right;
                    GameStateManager.scoreLeft++;
                    SoundManager.instance.PlaySuicideSound();
                    StartCoroutine("CalculateAnimationLength");
                    //Если хоть один ковбой остановился - запускается корутина WaitForShot() которая меняет GameStateManager.currentGameState. Нужно чтобы не менялся стейт если один ковбой себя подорвал
                }

            }
        }
	}

    

    IEnumerator CalculateAnimationLength()
    {
        yield return new WaitForEndOfFrame();
        bangAnimationLength = animator.GetCurrentAnimationTimeLength();
        //StartCoroutine("WaitForCurrentAnimationEnd");
        missShotController.SendMessage("StopWaitForShotCorutine");
        if (GameStateManager.scoreLeft == GameStateManager.maxScore || GameStateManager.scoreRight == GameStateManager.maxScore)
        {
            GameStateManager.currentGameState = (int)GameStateManager.DuelStates.end_game;
            //GameStateManager.needToSpawn = (int)GameStateManager.WhoToSpawn.both;
        }
    }

    //IEnumerator WaitForCurrentAnimationEnd()
    //{
    //    yield return new WaitForSeconds(bangAnimationLength);
    //    //missShotController.SendMessage("StopWaitForShotCorutine");
    //    suicide = false;
    //    if (GameStateManager.scoreLeft == GameStateManager.maxScore || GameStateManager.scoreRight == GameStateManager.maxScore)
    //    {
    //        GameStateManager.currentGameState = (int)GameStateManager.DuelStates.end_game;
    //        GameStateManager.needToSpawn = (int)GameStateManager.WhoToSpawn.both;
    //    }
    //}
}
