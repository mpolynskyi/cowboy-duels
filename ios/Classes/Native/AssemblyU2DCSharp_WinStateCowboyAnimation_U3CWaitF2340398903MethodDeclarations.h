﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WinStateCowboyAnimation/<WaitForRestart>c__Iterator5
struct U3CWaitForRestartU3Ec__Iterator5_t2340398903;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void WinStateCowboyAnimation/<WaitForRestart>c__Iterator5::.ctor()
extern "C"  void U3CWaitForRestartU3Ec__Iterator5__ctor_m1666317843 (U3CWaitForRestartU3Ec__Iterator5_t2340398903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WinStateCowboyAnimation/<WaitForRestart>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForRestartU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m944195257 (U3CWaitForRestartU3Ec__Iterator5_t2340398903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WinStateCowboyAnimation/<WaitForRestart>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForRestartU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m587967233 (U3CWaitForRestartU3Ec__Iterator5_t2340398903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WinStateCowboyAnimation/<WaitForRestart>c__Iterator5::MoveNext()
extern "C"  bool U3CWaitForRestartU3Ec__Iterator5_MoveNext_m986672837 (U3CWaitForRestartU3Ec__Iterator5_t2340398903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WinStateCowboyAnimation/<WaitForRestart>c__Iterator5::Dispose()
extern "C"  void U3CWaitForRestartU3Ec__Iterator5_Dispose_m3331584906 (U3CWaitForRestartU3Ec__Iterator5_t2340398903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WinStateCowboyAnimation/<WaitForRestart>c__Iterator5::Reset()
extern "C"  void U3CWaitForRestartU3Ec__Iterator5_Reset_m874998788 (U3CWaitForRestartU3Ec__Iterator5_t2340398903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
