﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"
#include "UnityEngine_UnityEngine_Vector2465617798.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PixelPerfectCamera
struct  PixelPerfectCamera_t2668956642  : public MonoBehaviour_t774292115
{
public:
	// UnityEngine.Vector2 PixelPerfectCamera::nativeResolution
	Vector2_t465617798  ___nativeResolution_4;

public:
	inline static int32_t get_offset_of_nativeResolution_4() { return static_cast<int32_t>(offsetof(PixelPerfectCamera_t2668956642, ___nativeResolution_4)); }
	inline Vector2_t465617798  get_nativeResolution_4() const { return ___nativeResolution_4; }
	inline Vector2_t465617798 * get_address_of_nativeResolution_4() { return &___nativeResolution_4; }
	inline void set_nativeResolution_4(Vector2_t465617798  value)
	{
		___nativeResolution_4 = value;
	}
};

struct PixelPerfectCamera_t2668956642_StaticFields
{
public:
	// System.Single PixelPerfectCamera::pixelsToUnits
	float ___pixelsToUnits_2;
	// System.Single PixelPerfectCamera::scale
	float ___scale_3;

public:
	inline static int32_t get_offset_of_pixelsToUnits_2() { return static_cast<int32_t>(offsetof(PixelPerfectCamera_t2668956642_StaticFields, ___pixelsToUnits_2)); }
	inline float get_pixelsToUnits_2() const { return ___pixelsToUnits_2; }
	inline float* get_address_of_pixelsToUnits_2() { return &___pixelsToUnits_2; }
	inline void set_pixelsToUnits_2(float value)
	{
		___pixelsToUnits_2 = value;
	}

	inline static int32_t get_offset_of_scale_3() { return static_cast<int32_t>(offsetof(PixelPerfectCamera_t2668956642_StaticFields, ___scale_3)); }
	inline float get_scale_3() const { return ___scale_3; }
	inline float* get_address_of_scale_3() { return &___scale_3; }
	inline void set_scale_3(float value)
	{
		___scale_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
