﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AnimationManager
struct AnimationManager_t2328214095;
// UnityEngine.GameObject
struct GameObject_t1366199518;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shot
struct  Shot_t3441902592  : public MonoBehaviour_t774292115
{
public:
	// AnimationManager Shot::animator
	AnimationManager_t2328214095 * ___animator_2;
	// UnityEngine.GameObject Shot::playerLeft
	GameObject_t1366199518 * ___playerLeft_3;
	// UnityEngine.GameObject Shot::playerRight
	GameObject_t1366199518 * ___playerRight_4;

public:
	inline static int32_t get_offset_of_animator_2() { return static_cast<int32_t>(offsetof(Shot_t3441902592, ___animator_2)); }
	inline AnimationManager_t2328214095 * get_animator_2() const { return ___animator_2; }
	inline AnimationManager_t2328214095 ** get_address_of_animator_2() { return &___animator_2; }
	inline void set_animator_2(AnimationManager_t2328214095 * value)
	{
		___animator_2 = value;
		Il2CppCodeGenWriteBarrier(&___animator_2, value);
	}

	inline static int32_t get_offset_of_playerLeft_3() { return static_cast<int32_t>(offsetof(Shot_t3441902592, ___playerLeft_3)); }
	inline GameObject_t1366199518 * get_playerLeft_3() const { return ___playerLeft_3; }
	inline GameObject_t1366199518 ** get_address_of_playerLeft_3() { return &___playerLeft_3; }
	inline void set_playerLeft_3(GameObject_t1366199518 * value)
	{
		___playerLeft_3 = value;
		Il2CppCodeGenWriteBarrier(&___playerLeft_3, value);
	}

	inline static int32_t get_offset_of_playerRight_4() { return static_cast<int32_t>(offsetof(Shot_t3441902592, ___playerRight_4)); }
	inline GameObject_t1366199518 * get_playerRight_4() const { return ___playerRight_4; }
	inline GameObject_t1366199518 ** get_address_of_playerRight_4() { return &___playerRight_4; }
	inline void set_playerRight_4(GameObject_t1366199518 * value)
	{
		___playerRight_4 = value;
		Il2CppCodeGenWriteBarrier(&___playerRight_4, value);
	}
};

struct Shot_t3441902592_StaticFields
{
public:
	// System.Boolean Shot::alreadyShot
	bool ___alreadyShot_5;
	// System.Boolean Shot::goToShotSoundPlayed
	bool ___goToShotSoundPlayed_6;

public:
	inline static int32_t get_offset_of_alreadyShot_5() { return static_cast<int32_t>(offsetof(Shot_t3441902592_StaticFields, ___alreadyShot_5)); }
	inline bool get_alreadyShot_5() const { return ___alreadyShot_5; }
	inline bool* get_address_of_alreadyShot_5() { return &___alreadyShot_5; }
	inline void set_alreadyShot_5(bool value)
	{
		___alreadyShot_5 = value;
	}

	inline static int32_t get_offset_of_goToShotSoundPlayed_6() { return static_cast<int32_t>(offsetof(Shot_t3441902592_StaticFields, ___goToShotSoundPlayed_6)); }
	inline bool get_goToShotSoundPlayed_6() const { return ___goToShotSoundPlayed_6; }
	inline bool* get_address_of_goToShotSoundPlayed_6() { return &___goToShotSoundPlayed_6; }
	inline void set_goToShotSoundPlayed_6(bool value)
	{
		___goToShotSoundPlayed_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
