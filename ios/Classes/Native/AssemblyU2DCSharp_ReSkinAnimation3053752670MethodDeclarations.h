﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReSkinAnimation
struct ReSkinAnimation_t3053752670;

#include "codegen/il2cpp-codegen.h"

// System.Void ReSkinAnimation::.ctor()
extern "C"  void ReSkinAnimation__ctor_m1158818905 (ReSkinAnimation_t3053752670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReSkinAnimation::Awake()
extern "C"  void ReSkinAnimation_Awake_m3510018730 (ReSkinAnimation_t3053752670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReSkinAnimation::LateUpdate()
extern "C"  void ReSkinAnimation_LateUpdate_m269496232 (ReSkinAnimation_t3053752670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
