﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t3921196294;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShowScore
struct  ShowScore_t4012426113  : public MonoBehaviour_t774292115
{
public:
	// UnityEngine.UI.Text ShowScore::leftScore
	Text_t3921196294 * ___leftScore_2;
	// UnityEngine.UI.Text ShowScore::rightScore
	Text_t3921196294 * ___rightScore_3;

public:
	inline static int32_t get_offset_of_leftScore_2() { return static_cast<int32_t>(offsetof(ShowScore_t4012426113, ___leftScore_2)); }
	inline Text_t3921196294 * get_leftScore_2() const { return ___leftScore_2; }
	inline Text_t3921196294 ** get_address_of_leftScore_2() { return &___leftScore_2; }
	inline void set_leftScore_2(Text_t3921196294 * value)
	{
		___leftScore_2 = value;
		Il2CppCodeGenWriteBarrier(&___leftScore_2, value);
	}

	inline static int32_t get_offset_of_rightScore_3() { return static_cast<int32_t>(offsetof(ShowScore_t4012426113, ___rightScore_3)); }
	inline Text_t3921196294 * get_rightScore_3() const { return ___rightScore_3; }
	inline Text_t3921196294 ** get_address_of_rightScore_3() { return &___rightScore_3; }
	inline void set_rightScore_3(Text_t3921196294 * value)
	{
		___rightScore_3 = value;
		Il2CppCodeGenWriteBarrier(&___rightScore_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
