﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MissShotController/<waitForBangAnimationEnds>c__Iterator2
struct U3CwaitForBangAnimationEndsU3Ec__Iterator2_t3907694863;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MissShotController/<waitForBangAnimationEnds>c__Iterator2::.ctor()
extern "C"  void U3CwaitForBangAnimationEndsU3Ec__Iterator2__ctor_m3129254197 (U3CwaitForBangAnimationEndsU3Ec__Iterator2_t3907694863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MissShotController/<waitForBangAnimationEnds>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CwaitForBangAnimationEndsU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2231319611 (U3CwaitForBangAnimationEndsU3Ec__Iterator2_t3907694863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MissShotController/<waitForBangAnimationEnds>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CwaitForBangAnimationEndsU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2526608723 (U3CwaitForBangAnimationEndsU3Ec__Iterator2_t3907694863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MissShotController/<waitForBangAnimationEnds>c__Iterator2::MoveNext()
extern "C"  bool U3CwaitForBangAnimationEndsU3Ec__Iterator2_MoveNext_m2590649983 (U3CwaitForBangAnimationEndsU3Ec__Iterator2_t3907694863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissShotController/<waitForBangAnimationEnds>c__Iterator2::Dispose()
extern "C"  void U3CwaitForBangAnimationEndsU3Ec__Iterator2_Dispose_m3395930938 (U3CwaitForBangAnimationEndsU3Ec__Iterator2_t3907694863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissShotController/<waitForBangAnimationEnds>c__Iterator2::Reset()
extern "C"  void U3CwaitForBangAnimationEndsU3Ec__Iterator2_Reset_m2012661448 (U3CwaitForBangAnimationEndsU3Ec__Iterator2_t3907694863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
