﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AnimationManager
struct AnimationManager_t2328214095;
// System.Collections.IEnumerator
struct IEnumerator_t3037427797;
// UnityEngine.GameObject
struct GameObject_t1366199518;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DuelBeforeShotAnimation
struct  DuelBeforeShotAnimation_t4264097629  : public MonoBehaviour_t774292115
{
public:
	// AnimationManager DuelBeforeShotAnimation::animator
	AnimationManager_t2328214095 * ___animator_3;
	// UnityEngine.GameObject DuelBeforeShotAnimation::missShotController
	GameObject_t1366199518 * ___missShotController_6;
	// UnityEngine.GameObject DuelBeforeShotAnimation::playerLeft
	GameObject_t1366199518 * ___playerLeft_7;
	// UnityEngine.GameObject DuelBeforeShotAnimation::playerRight
	GameObject_t1366199518 * ___playerRight_8;

public:
	inline static int32_t get_offset_of_animator_3() { return static_cast<int32_t>(offsetof(DuelBeforeShotAnimation_t4264097629, ___animator_3)); }
	inline AnimationManager_t2328214095 * get_animator_3() const { return ___animator_3; }
	inline AnimationManager_t2328214095 ** get_address_of_animator_3() { return &___animator_3; }
	inline void set_animator_3(AnimationManager_t2328214095 * value)
	{
		___animator_3 = value;
		Il2CppCodeGenWriteBarrier(&___animator_3, value);
	}

	inline static int32_t get_offset_of_missShotController_6() { return static_cast<int32_t>(offsetof(DuelBeforeShotAnimation_t4264097629, ___missShotController_6)); }
	inline GameObject_t1366199518 * get_missShotController_6() const { return ___missShotController_6; }
	inline GameObject_t1366199518 ** get_address_of_missShotController_6() { return &___missShotController_6; }
	inline void set_missShotController_6(GameObject_t1366199518 * value)
	{
		___missShotController_6 = value;
		Il2CppCodeGenWriteBarrier(&___missShotController_6, value);
	}

	inline static int32_t get_offset_of_playerLeft_7() { return static_cast<int32_t>(offsetof(DuelBeforeShotAnimation_t4264097629, ___playerLeft_7)); }
	inline GameObject_t1366199518 * get_playerLeft_7() const { return ___playerLeft_7; }
	inline GameObject_t1366199518 ** get_address_of_playerLeft_7() { return &___playerLeft_7; }
	inline void set_playerLeft_7(GameObject_t1366199518 * value)
	{
		___playerLeft_7 = value;
		Il2CppCodeGenWriteBarrier(&___playerLeft_7, value);
	}

	inline static int32_t get_offset_of_playerRight_8() { return static_cast<int32_t>(offsetof(DuelBeforeShotAnimation_t4264097629, ___playerRight_8)); }
	inline GameObject_t1366199518 * get_playerRight_8() const { return ___playerRight_8; }
	inline GameObject_t1366199518 ** get_address_of_playerRight_8() { return &___playerRight_8; }
	inline void set_playerRight_8(GameObject_t1366199518 * value)
	{
		___playerRight_8 = value;
		Il2CppCodeGenWriteBarrier(&___playerRight_8, value);
	}
};

struct DuelBeforeShotAnimation_t4264097629_StaticFields
{
public:
	// System.Boolean DuelBeforeShotAnimation::waitForShotStarted
	bool ___waitForShotStarted_2;
	// System.Single DuelBeforeShotAnimation::bangAnimationLength
	float ___bangAnimationLength_4;
	// System.Collections.IEnumerator DuelBeforeShotAnimation::waitForShot
	Il2CppObject * ___waitForShot_5;
	// System.Boolean DuelBeforeShotAnimation::suicide
	bool ___suicide_9;

public:
	inline static int32_t get_offset_of_waitForShotStarted_2() { return static_cast<int32_t>(offsetof(DuelBeforeShotAnimation_t4264097629_StaticFields, ___waitForShotStarted_2)); }
	inline bool get_waitForShotStarted_2() const { return ___waitForShotStarted_2; }
	inline bool* get_address_of_waitForShotStarted_2() { return &___waitForShotStarted_2; }
	inline void set_waitForShotStarted_2(bool value)
	{
		___waitForShotStarted_2 = value;
	}

	inline static int32_t get_offset_of_bangAnimationLength_4() { return static_cast<int32_t>(offsetof(DuelBeforeShotAnimation_t4264097629_StaticFields, ___bangAnimationLength_4)); }
	inline float get_bangAnimationLength_4() const { return ___bangAnimationLength_4; }
	inline float* get_address_of_bangAnimationLength_4() { return &___bangAnimationLength_4; }
	inline void set_bangAnimationLength_4(float value)
	{
		___bangAnimationLength_4 = value;
	}

	inline static int32_t get_offset_of_waitForShot_5() { return static_cast<int32_t>(offsetof(DuelBeforeShotAnimation_t4264097629_StaticFields, ___waitForShot_5)); }
	inline Il2CppObject * get_waitForShot_5() const { return ___waitForShot_5; }
	inline Il2CppObject ** get_address_of_waitForShot_5() { return &___waitForShot_5; }
	inline void set_waitForShot_5(Il2CppObject * value)
	{
		___waitForShot_5 = value;
		Il2CppCodeGenWriteBarrier(&___waitForShot_5, value);
	}

	inline static int32_t get_offset_of_suicide_9() { return static_cast<int32_t>(offsetof(DuelBeforeShotAnimation_t4264097629_StaticFields, ___suicide_9)); }
	inline bool get_suicide_9() const { return ___suicide_9; }
	inline bool* get_address_of_suicide_9() { return &___suicide_9; }
	inline void set_suicide_9(bool value)
	{
		___suicide_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
