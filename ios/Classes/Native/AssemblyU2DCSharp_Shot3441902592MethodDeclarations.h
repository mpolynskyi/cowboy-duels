﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Shot
struct Shot_t3441902592;
// System.Collections.IEnumerator
struct IEnumerator_t3037427797;

#include "codegen/il2cpp-codegen.h"

// System.Void Shot::.ctor()
extern "C"  void Shot__ctor_m1262883755 (Shot_t3441902592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shot::.cctor()
extern "C"  void Shot__cctor_m408840314 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shot::Start()
extern "C"  void Shot_Start_m850548415 (Shot_t3441902592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shot::Update()
extern "C"  void Shot_Update_m3469805538 (Shot_t3441902592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Shot::WaitForSpawnNewCowboy()
extern "C"  Il2CppObject * Shot_WaitForSpawnNewCowboy_m432840889 (Shot_t3441902592 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
