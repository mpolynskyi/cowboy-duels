﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AnimationManager
struct AnimationManager_t2328214095;

#include "UnityEngine_UnityEngine_MonoBehaviour774292115.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WinStateCowboyAnimation
struct  WinStateCowboyAnimation_t2747849222  : public MonoBehaviour_t774292115
{
public:
	// AnimationManager WinStateCowboyAnimation::animator
	AnimationManager_t2328214095 * ___animator_2;

public:
	inline static int32_t get_offset_of_animator_2() { return static_cast<int32_t>(offsetof(WinStateCowboyAnimation_t2747849222, ___animator_2)); }
	inline AnimationManager_t2328214095 * get_animator_2() const { return ___animator_2; }
	inline AnimationManager_t2328214095 ** get_address_of_animator_2() { return &___animator_2; }
	inline void set_animator_2(AnimationManager_t2328214095 * value)
	{
		___animator_2 = value;
		Il2CppCodeGenWriteBarrier(&___animator_2, value);
	}
};

struct WinStateCowboyAnimation_t2747849222_StaticFields
{
public:
	// System.Boolean WinStateCowboyAnimation::waitForWinStarted
	bool ___waitForWinStarted_3;

public:
	inline static int32_t get_offset_of_waitForWinStarted_3() { return static_cast<int32_t>(offsetof(WinStateCowboyAnimation_t2747849222_StaticFields, ___waitForWinStarted_3)); }
	inline bool get_waitForWinStarted_3() const { return ___waitForWinStarted_3; }
	inline bool* get_address_of_waitForWinStarted_3() { return &___waitForWinStarted_3; }
	inline void set_waitForWinStarted_3(bool value)
	{
		___waitForWinStarted_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
