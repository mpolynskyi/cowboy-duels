﻿using UnityEngine;
using System.Collections;

public class StartGame : MonoBehaviour {
    static GameObject tapText;
    public AudioClip startMusic;
    bool musicStarted = false;
    void Start()
    {
        tapText = gameObject;
    }
	void Update () {
	    if(GameStateManager.currentGameState == (int)GameStateManager.DuelStates.start_game)
        {
            if (!musicStarted)
            {
                SoundManager.instance.RandomizeSfx(startMusic);
                musicStarted = true;
            }
            

            if (Input.GetMouseButtonDown(0))
            {
                tapText.SetActive(false);
                GameStateManager.currentGameState = (int)GameStateManager.DuelStates.spawn_cowboy;
                musicStarted = false;
            }
        }
	}

    public static void RestartGame()
    {
        GameStateManager.currentGameState = (int)GameStateManager.DuelStates.start_game;
        //GameStateManager.needToSpawn = (int)GameStateManager.WhoToSpawn.both;
        GameStateManager.scoreLeft = 0;
        GameStateManager.scoreRight = 0;
        tapText.SetActive(true);
        Shot.goToShotSoundPlayed = false;
    }
}
