﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MissShotController
struct MissShotController_t1066454504;
// System.Collections.IEnumerator
struct IEnumerator_t3037427797;

#include "codegen/il2cpp-codegen.h"

// System.Void MissShotController::.ctor()
extern "C"  void MissShotController__ctor_m3541477155 (MissShotController_t1066454504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissShotController::.cctor()
extern "C"  void MissShotController__cctor_m2397078434 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissShotController::Update()
extern "C"  void MissShotController_Update_m890413674 (MissShotController_t1066454504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MissShotController::WaitForShotCorutine()
extern "C"  Il2CppObject * MissShotController_WaitForShotCorutine_m378600622 (MissShotController_t1066454504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MissShotController::StopWaitForShotCorutine()
extern "C"  void MissShotController_StopWaitForShotCorutine_m665378014 (MissShotController_t1066454504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MissShotController::waitForBangAnimationEnds()
extern "C"  Il2CppObject * MissShotController_waitForBangAnimationEnds_m1798326465 (MissShotController_t1066454504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
