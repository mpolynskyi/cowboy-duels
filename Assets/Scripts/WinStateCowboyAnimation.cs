﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WinStateCowboyAnimation : MonoBehaviour {
    AnimationManager animator;
    public static bool waitForWinStarted = false;

    void Start()
    {
        animator = GetComponent<AnimationManager>();
    }
    
	void Update ()
    {
	    if(GameStateManager.currentGameState == (int)GameStateManager.DuelStates.end_game)
        {

            if (!waitForWinStarted)
            {
                StartCoroutine("WaitForWin");    //Wait for start win animation
                waitForWinStarted = true;
            }

        }
	}

    IEnumerator WaitForWin()
    {
        yield return new WaitForSeconds(2);
        SoundManager.instance.PlayWinStateMusic();
        animator.Win();
        StartCoroutine("WaitForRestart");
    }

    IEnumerator WaitForRestart()
    {
        yield return new WaitForSeconds(3);
        waitForWinStarted = false;  //reset value to next win state
        StartGame.RestartGame();
    }
}
