﻿using UnityEngine;

public class AnimationManager : MonoBehaviour {

    protected Animator animator;

    enum MyAnimations { walk, idle, idle_with_gun, shot, bang, win };

    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void Walk()
    {
        animator.SetInteger("state", (int)MyAnimations.walk);
    }

    public void Idle()
    {
        animator.SetInteger("state", (int)MyAnimations.idle);
    }

    public void IdleWithGun()
    {
        animator.SetInteger("state", (int)MyAnimations.idle_with_gun);
    }

    public void Shot()
    {
        animator.SetInteger("state", (int)MyAnimations.shot);
    }

    public void Bang()
    {
        animator.SetInteger("state", (int)MyAnimations.bang);
    }

    public void Win()
    {
        animator.SetInteger("state", (int)MyAnimations.win);
    }

    public float GetCurrentAnimationTimeLength()
    {
        return animator.GetCurrentAnimatorStateInfo(0).length;
    }

}
