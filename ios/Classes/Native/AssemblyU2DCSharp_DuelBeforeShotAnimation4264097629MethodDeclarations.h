﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DuelBeforeShotAnimation
struct DuelBeforeShotAnimation_t4264097629;
// System.Collections.IEnumerator
struct IEnumerator_t3037427797;

#include "codegen/il2cpp-codegen.h"

// System.Void DuelBeforeShotAnimation::.ctor()
extern "C"  void DuelBeforeShotAnimation__ctor_m1957131436 (DuelBeforeShotAnimation_t4264097629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DuelBeforeShotAnimation::.cctor()
extern "C"  void DuelBeforeShotAnimation__cctor_m768009431 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DuelBeforeShotAnimation::Start()
extern "C"  void DuelBeforeShotAnimation_Start_m2208841316 (DuelBeforeShotAnimation_t4264097629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DuelBeforeShotAnimation::Update()
extern "C"  void DuelBeforeShotAnimation_Update_m2108688449 (DuelBeforeShotAnimation_t4264097629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DuelBeforeShotAnimation::CalculateAnimationLength()
extern "C"  Il2CppObject * DuelBeforeShotAnimation_CalculateAnimationLength_m1743222608 (DuelBeforeShotAnimation_t4264097629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
