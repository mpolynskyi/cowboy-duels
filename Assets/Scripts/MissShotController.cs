﻿using UnityEngine;
using System.Collections;

public class MissShotController : MonoBehaviour
{
    public static bool waitForShotStarted = false;
    GameObject playerLeft;
    GameObject playerRight;
    bool WaitForSpawnNewCowboyStarted = false;


    void Update()
    {
        if (playerLeft == null)
        {
            playerLeft = GameObject.FindWithTag("PlayerLeft");
        }
        if (playerRight == null)
        {
            playerRight = GameObject.FindWithTag("PlayerRight");
        }

        if (GameStateManager.currentGameState == (int)GameStateManager.DuelStates.cowboys_ready)
        {

            if (!waitForShotStarted)
            {
                StartCoroutine("WaitForShotCorutine");
                waitForShotStarted = true;
            }
        }
    }

    IEnumerator WaitForShotCorutine()
    {
        print("WaitForShotCorutine started");
        yield return new WaitForSeconds(GameStateManager.RandomDelay);
        print("WaitForShotCorutine подождала и теперь разруливает");

        //if first cowboy ready for duel, second is ready too. Second cowboy not set game statemant to cowboys_aim_guns again.
        if (GameStateManager.CowboysOnScene == 2)
        {
            GameStateManager.currentGameState = (int)GameStateManager.DuelStates.cowboys_aim_guns;
            playerLeft.SendMessage("IdleWithGun");
            playerRight.SendMessage("IdleWithGun");
        }

        waitForShotStarted = false;      // reset for next spawned cowboys
    }
    public void StopWaitForShotCorutine()
    {
        StopCoroutine("WaitForShotCorutine");
        StartCoroutine("waitForBangAnimationEnds");  
    }

    IEnumerator waitForBangAnimationEnds()
    {
        yield return new WaitForSeconds(DuelBeforeShotAnimation.bangAnimationLength);
        if (GameStateManager.currentGameState != (int)GameStateManager.DuelStates.end_game)
        {
            GameStateManager.currentGameState = (int)GameStateManager.DuelStates.spawn_cowboy;
        }
        DuelBeforeShotAnimation.suicide = false;
        waitForShotStarted = false;
    }
}
